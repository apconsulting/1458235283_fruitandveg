//
//  AWUnit.h
//  AdWowSDK
//
//  Created by heximal on 17/08/14.
//  Copyright (c) 2014 heximal. All rights reserved.
//

#import <Foundation/Foundation.h>


#import "AWNotification.h"
#import "AWForm.h"

@protocol AWUnitDelegate;

/**
 Используйте AWUnit для отображения модулей таких как Награда пользователю
 */
@interface AWUnit : NSObject

/** @name Свойства Модуля */

/**
 Уникальный идентификатор модуля.
 */
@property (strong, nonatomic) NSString *unitId;

/**
 Нотификация модуля.
 */
@property (strong, nonatomic) AWNotification *notification;

/**
 Форма модуля.
 */
@property (strong, nonatomic) AWForm *form;


/** @name Геттеры и сеттеры делегата */

/**
 Делегат объекта Модуль.
 */
@property (assign, nonatomic) id<AWUnitDelegate> delegate;


/** @name Показ Модуля */

/**
 Производит показ модуля.
 */
- (void) show;

@end


/**
 Протокол AWUnitDelegate определяет методы делегата класса AWUnit.
 Данный делегат реализует методы уведомления такие как показ и скрытие
 */
@protocol AWUnitDelegate <NSObject>

/** @name Управление Модулем */

@optional

/**
 Сообщает делегату, что Модуль будет представлен пользователю
 
 @param unit Модуль, который будет представлен.
 */
- (void) willPresentUnit:(AWUnit *)unit;

/**
 Сообщает делегату, что Модуль скрыт
 
 @param unit Модуль, который будет скрыт.
 */
- (void) didDismissUnit:(AWUnit *)unit;

@end
