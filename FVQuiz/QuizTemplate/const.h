


#define AdMob_ID @"ca-app-pub-1787943099835708/6987128307"
#define AdIntMob_ID @"ca-app-pub-1787943099835708/7502021904"
#define kGoogleAnaliticsAppKey @"UA-41896275-15"

#define TapJoyConnect @"96143971-a6e0-4079-b577-d991e723bd91"
#define TapJoyKey @"MBRyXqOnmxuukzBCs7Hk"

#define kLeaderboardID @"quiz_wc"
#define kLeaderboardOfflineEasyID @"1234_easy"
#define kLeaderboardOfflineMediumID @"1234_medium"
#define kLeaderboardOfflineHardID @"1234_hard"
#define ChartboostAppId @"55d4f3995b1453121afad35d"
#define ChartboostSignature @"fab459f1940edb84a471dcc8b7bcba63b3c474a5"


#define iTunesLink @"https://itunes.apple.com/app/id1032014975"
#define iTunedID @"1032014975"

#define FlurryAppId @"YX8WVNVRYW83XFHV7QBB"

#define urlENG @"http://iosgames.ru/?page_id=15554"
#define urlRUS @"http://iosgames.ru/?page_id=15554"

#define urlVK @"https://vk.com/iosgamesru"
#define urlTW @"https://twitter.com/Game4iOS"
#define urlFB @"https://www.facebook.com/game4ios"
#define urlOtherQuez @"http://iosgames.ru/games.html"

#define VkontakteAppId_ @"5129062"
#define FacebookAppId_  @"892427830846435"

//static NSInteger vkAuthErnedCoins = 0;
//static NSInteger vkShareErnedCoins = 40;
//static NSInteger vkSibscribeErnedCoins = 20;
//static NSInteger vkInviteFriendErnedCoins = 60;
//
//static NSString *const urlAppForVK = @"https://itunes.apple.com/app/apple-store/id1032014975?pt=879222&ct=1234";//@"https://itunes.apple.com/us/app/car-quiz!/id804942069?mt=8";
//static NSString *const shareLinkTextToVK = @"Ссылка в App Store:";
//static NSString *const iTunesLinkRu = @"https://itunes.apple.com/ru/app/apple-store/id1032014975?pt=879222&ct=1234";
//static NSString *const googlePlayLinkRu = @"https://goo.gl/bDQVJE";
//static NSString *const VkAppID = @"5129062";
//
//
//
//static NSString *const AdToAppID = @"e1c4c0f6-668c-4010-8aef-4336a3eb5b3a:11ed797d-7c6d-4264-93f2-6c64adda009f";
//static NSString *const shareImageName = @"share974.png";
//static NSInteger fbShareErnedCoins = 40;
//static NSInteger twShareErnedCoins = 40;
//static NSCalendarUnit const coinsLocalPushInterval = NSDayCalendarUnit;//раз в день
//static NSCalendarUnit const vkLocalPushInterval = NSWeekCalendarUnit;//раз в неделю
//static NSCalendarUnit const videoLocalPushInterval = NSWeekCalendarUnit;//раз в неделю
//
//static NSTimeInterval const vkLocalPushOffset = 3600 * 24 * 5 + 120; //пять дней + 2 часа
//static NSTimeInterval const videoLocalPushOffset = 3600 * 24 * 3 + 120; //три дня + 2 часа



//settings color
//#define colorBackgroundBaseView [UIColor colorWith8BitRed:190 green:226 blue:222 alpha:1];
#define colorBackgroundBaseView [UIColor colorWith8BitRed:161 green:230 blue:229 alpha:1];

//FONT settings -------------///
#define fontName @"Trebuchet MS"

#define fSizePhone 12
#define fSizePad 24

//main
#define fSizePadMainLbStartGame 15
#define fSizePadMainLbStartOnline 12
#define fSizePadMainLbCoins 24
#define fSizePhoneMainLbStartGame 15
#define fSizePhoneMainLbStartOnline 12
#define fSizePhoneMainLbCoins 12

//game
#define fSizePadGame 30
#define fSizePhoneGame 15

//getCoints
#define fSizePadGetCoins 24
#define fSizePhoneGetCoins 12
#define fSizePadGetCoins2 20
#define fSizePhoneGetCoins2 10

//QuestionView
#define fSizePadQuestionLbl 74
#define fSizePhoneQuestionLbl 36

//win alert
#define fSizePad_lbCoins 45
#define fSizePad_lbCoins2 45
#define fSizePad_lbVictory 50
#define fSizePad_btnOk 40

#define fSizePhone_lbCoins 25
#define fSizePhone_lbCoins2 25
#define fSizePhone_lbVictory 30
#define fSizePhone_btnOk 40
//------------------///

//settings game

#define LevelsCount 1
#define TimerValueEasyLevel 6
#define TimerValueMediumLevel 3
#define TimerValueHardLevel 3
#define BotTimer 20

#define totalCountQuestions 300

#define startCoints 300
#define RightPrice 0
#define WrongPrice 1
#define WrongAnswerPrice 1
#define RemoveAllCellsPrice 20


//////////////////

#define colorBackgroundSubsribeForIpad [UIColor colorWithWhite:0.0 alpha:0.5]
#define AppEmperorServerID 0
#define AppEmperorBannerID_EN 0
#define AppEmperorBannerID_RU 0
#define AppEmeprorAlertID 0
