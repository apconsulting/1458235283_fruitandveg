//
//  AppDelegate.m
//  QuizTemplate
//
//  Created by Uladzislau Yasnitski on 11/12/13.
//  Copyright (c) 2013 Uladzislau Yasnitski. All rights reserved.
//

#import "AppDelegate.h"
#import "AppSettings.h"
#import "Localization.h"
#import "MKStoreManager.h"
#import "LoginManager.h"
//#import <FacebookSDK/FacebookSDK.h>
#import "MainViewController.h"
#import "MainViewController_iPhone.h"
#import "MainViewController_iPad.h"
#import <Chartboost/Chartboost.h>
#import "Model.h"
#import <Tapjoy/Tapjoy.h>
#import "CRFileUtils.h"
#import "GameModel.h"
#import "Flurry.h"
#import "ShareKit.h"
#import "SHKConfiguration.h"
#import "ShareKitDemoConfigurator.h"
//#import "SHKVkontakte.h"
#import "PresentCoinsManager.h"
#import <ShareKit/SharersCommonHeaders.h>
#import <AdWowSDK/AdWowSDK.h>
#import <UnityAds/UnityAds.h>
#import "ConstantsAndMacros.h"

#import <UserNotifications/UserNotifications.h>

#import "VKSdk.h"

#import "AdToAppSDK.h"
#import "ATALog.h"
#import <YandexMobileMetrica/YMMYandexMetrica.h>
#import <YandexMobileMetrica/YMMYandexMetricaConfiguration.h>
#import <YandexMobileAds/YandexMobileAds.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>


#import <UserNotifications/UserNotifications.h>
#import "AVOSCloud.h"
#import <Bugly/Bugly.h>

#import "SSbtvAEJob62c0sYLIXsJpvCAA.h"


//#import <TwitterKit/TWTRKit.h>

@import Firebase;
@import UserNotifications;


@interface AppDelegate ()<AdWowDelegate, ChartboostDelegate, VKSdkDelegate, UIAlertViewDelegate, AdToAppSDKDelegate, GADInterstitialDelegate, YMAInterstitialDelegate, TJPlacementDelegate, UNUserNotificationCenterDelegate>{
    GADInterstitial *_interstitial;
}

@property(nonatomic, strong)UILocalNotification *notification;
@property(nonatomic, strong) UNNotification *un_Notification;
@property(nonatomic, assign) BOOL isFirstLaunch;
@property (nonatomic, strong) YMAInterstitialController *interstitialAd;

@end

@implementation AppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    //Push
    //if (!TARGET_IPHONE_SIMULATOR)
    [self initPush];
    
    [AVOSCloud setApplicationId:@"KY07m8TIkWFYFl1E3mwfuovU-MdYXbMMI" clientKey:@"hBfwsgrklPFOQzgvTxgCJVoG"];
    
    [AVOSCloud setAllLogsEnabled:YES];
    
    [AVAnalytics setChannel:@"AppStore"];
    
    [AVAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    
    [self registerForRemoteNotification];
    
    [Bugly startWithAppId:@"i31l4hihBfwsgrklPFOQzgvTxgCJVoG"];
    
    
    
    [self loadDatabase];
    
    DefaultSHKConfigurator *configurator = [[ShareKitDemoConfigurator alloc] init];
    [SHKConfiguration sharedInstanceWithConfigurator:configurator];
    
    [SHK flushOfflineQueue];
    
    [[LoginManager sharedInstance] appLaunched];
    
    [self initAddons];
    [FIRApp configure];     
    [[MKStoreManager sharedManager] purchasableObjectsDescription];
    
    [application setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    
    //        [application setIdleTimerDisabled:YES];
    application.applicationIconBadgeNumber = 0;
    
    MainViewController *vc = nil;
    if (isPad)
    {
        vc = [[MainViewController_iPad alloc] initWithNibName:@"MainViewController_iPad" bundle:nil];
    }
    else
    {
        vc = [[MainViewController_iPhone alloc] initWithNibName:@"MainViewController_iPhone" bundle:nil];
    }
    
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:vc];
    self.navigationController.navigationBarHidden = YES;
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = self.navigationController;
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    NSDictionary *remoteNotif = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if(remoteNotif) {
        [self handleNotification:remoteNotif];
    }
    self.notification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
//    [[Twitter sharedInstance] startWithConsumerKey:@"gB3EBzkGcdqClJBbl0BtJCsyL" consumerSecret:@"wX2SHuifhvW777Jqd2lshyNArLYwcC0utl2353LB1Yz8g4U9hC"];
    return YES;
    
}


/**
 * 初始化UNUserNotificationCenter
 */
- (void)registerForRemoteNotification {
    // iOS10 兼容
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 10.0) {
        // 使用 UNUserNotificationCenter 来管理通知
        UNUserNotificationCenter *uncenter = [UNUserNotificationCenter currentNotificationCenter];
        // 监听回调事件
        [uncenter setDelegate:self];
        //iOS10 使用以下方法注册，才能得到授权
        [uncenter requestAuthorizationWithOptions:(UNAuthorizationOptionAlert+UNAuthorizationOptionBadge+UNAuthorizationOptionSound)
                                completionHandler:^(BOOL granted, NSError * _Nullable error) {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        [[UIApplication sharedApplication] registerForRemoteNotifications];
                                        
                                    });
                                    
                                    //TODO:授权状态改变
                                    NSLog(@"%@" , granted ? @"auth success" : @"auth error");
                                }];
        // 获取当前的通知授权状态, UNNotificationSettings
        [uncenter getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
            NSLog(@"%s\nline:%@\n-----\n%@\n\n", __func__, @(__LINE__), settings);
            /*
             UNAuthorizationStatusNotDetermined : 没有做出选择
             UNAuthorizationStatusDenied : 用户未授权
             UNAuthorizationStatusAuthorized ：用户已授权
             */
            if (settings.authorizationStatus == UNAuthorizationStatusNotDetermined) {
                NSLog(@"no choose");
            } else if (settings.authorizationStatus == UNAuthorizationStatusDenied) {
                NSLog(@"no auth");
            } else if (settings.authorizationStatus == UNAuthorizationStatusAuthorized) {
                NSLog(@"authed");
            }
        }];
    }
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
        UIUserNotificationType types = UIUserNotificationTypeAlert |
        UIUserNotificationTypeBadge |
        UIUserNotificationTypeSound;
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    } else {
        UIRemoteNotificationType types = UIRemoteNotificationTypeBadge |
        UIRemoteNotificationTypeAlert |
        UIRemoteNotificationTypeSound;
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:types];
    }
#pragma clang diagnostic pop
    
    
}



- (void)initAddons {

    AdWow * adWow = [[AdWow sharedInstance] initWithAppKey:AW_APP_KEY andSecret:AW_APP_SECRET];
    adWow.delegate = self;

    [Flurry setCrashReportingEnabled:YES];
    [Flurry startSession:FlurryAppId];

    [AdToAppSDK startWithAppId:@"87ec17e5-db26-4e49-9464-5e1e17b42c8e:a658b6ce-608f-40ed-9f3a-217cb17caf4e" modules:@[
                                                                                                                      ADTOAPP_IMAGE_INTERSTITIAL,
                                                                                                                      ADTOAPP_REWARDED_INTERSTITIAL,
                                                                                                                 ]];
    [Chartboost startWithAppId:ChartboostAppId appSignature:ChartboostSignature delegate:self];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tjcConnectSuccess:)
                                                 name:TJC_CONNECT_SUCCESS
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tjcConnectFail:)
                                                 name:TJC_CONNECT_FAILED
                                               object:nil];

    //Turn on Tapjoy debug mode
    [Tapjoy setDebugEnabled:YES]; //Do not set this for any ver
    [Tapjoy connect:TapJoyConnect];
    [Tapjoy getCurrencyBalance];

    [[UnityAds sharedInstance] startWithGameId:@"1630169"];

    
    VKSdk *_VKSdk = [VKSdk initializeWithAppId:VkAppID];
    [_VKSdk registerDelegate:self];
    [_VKSdk setUiDelegate:self];

// VKSdk *sdkInstance = [VKSdk initializeWithAppId:VkAppID];
//    [sdkInstance registerDelegate:delegate];
//    [sdkInstance setUiDelegate:uiDelegate];
    
    [VKSdk wakeUpSession:@[VK_PER_WALL, VK_PER_EMAIL, VK_PER_GROUPS] completeBlock:^(VKAuthorizationState authorizationState, NSError *error) {
        //NSLog(@"wakeUpSession %d", authorizationState);
    }];
    
//    [VKSdk initializeWithDelegate:self andAppId:VkAppID];
//    [VKSdk wakeUpSession];
}

- (void)loadDatabase {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"everLaunched"]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"everLaunched"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstLaunch"];
    } else {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"firstLaunch"];
    }
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunch"] || ![AppSettings  dataImported]) {
        
        NSString *currentLang = [self systemLanguage];
        
        [Localization instance].locale = [[NSLocale alloc] initWithLocaleIdentifier:currentLang];
        [AppSettings  setCurrentLanguage:currentLang];
        [AppSettings  setAppVersion:@"2.0"];
        [AppSettings  setAdBannerLink:@"http://banner.link"];
        
        [[MKStoreManager sharedManager] removeAllKeychainData];
        
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        
    } else  {
        NSString *lang = [self systemLanguage];
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:lang] ;
        [[Localization instance] setLocale:locale];
        [AppSettings setCurrentLanguage:lang];
        
        if ([[AppSettings appVersion] floatValue] < 2.0) {
            [AppSettings setDataImported:NO];
            [AppSettings setAppVersion:@"2.0"];
            
//            [[UIApplication sharedApplication] cancelAllLocalNotifications];
        }
        
    }
}

#pragma makr - VK
- (void)updateCoins {
    BaseViewController *controller = (BaseViewController*)((UINavigationController*)self.window.rootViewController).topViewController;
    if ([controller isKindOfClass:[BaseViewController class]]) {
        [controller updateCoinsForLabel:controller.lbCoins withAnimation:NO];
    }
}

- (NSString*)shareString {
    NSString *text = @"Я играю в Угадай авто! Отличная викторина! Присоединяйтесь!\nСсылка в App Store: https://vk.cc/60pais\nСсылка в Google Play: https://vk.cc/60pfvv\nПодробнее: http://iosgames.ru/ugaday-avto-viktorina-dlya-znatokov/";
    
    return text;
}

- (void)shareToVK {
    VKUploadImage *vkImage = [VKUploadImage uploadImageWithImage:[UIImage imageNamed:@"share.jpg"] andParams:VKImageTypeJpg];
    
    VKShareDialogController * shareDialog = [VKShareDialogController new]; //1
    shareDialog.text         = [self shareString]; //2
    shareDialog.uploadImages = @[vkImage];
    //    shareDialog.shareLink    = [[VKShareLink alloc] initWithTitle:@""
    //                                                             link:[NSURL URLWithString:@"http://iosgames.ru/banners/share974.png"]]; //4
    [shareDialog setCompletionHandler:^(VKShareDialogController *dialog, VKShareDialogControllerResult result) {
        if (result == VKShareDialogControllerResultDone) {
            [Flurry logEvent:@"VK - Поделился"];
            [FIRAnalytics logEventWithName:@"Share_VK" parameters:nil];
            [YMMYandexMetrica reportEvent:@"Share_VK" parameters:nil
                                onFailure:^(NSError * _Nonnull error) {}];
            [[GameModel sharedInstance] earnCoins:vkShareErnedCoins];
            [self updateCoins];
        }
        [self.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
    }];
    [self.window.rootViewController presentViewController:shareDialog animated:YES completion:nil];
}

- (BOOL)authorizeVK {
    if ([VKSdk isLoggedIn]) {
        return NO;
    }
    [VKSdk authorize:@[@"friends", @"wall", @"groups", @"photos"]];
    return YES;
}

- (BOOL)vkSdkIsBasicAuthorization {
    return YES;
}

- (void)vkSdkAcceptedUserToken:(VKAccessToken *)token {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"vkDidAuthorized" object:nil];
}

- (void)vkSdkRenewedToken:(VKAccessToken *)newToken {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"vkDidAuthorized" object:nil];
}

- (void)vkSdkReceivedNewToken:(VKAccessToken*) newToken {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"vkDidAuthorized" object:nil];
}

- (void)vkSdkUserDeniedAccess:(VKError*) authorizationError {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"vkDidAuthorized" object:nil];
}

- (void)vkSdkNeedCaptchaEnter:(VKError*) captchaError {
    VKCaptchaViewController * vc = [VKCaptchaViewController captchaControllerWithError:captchaError];
    [vc presentIn:self.window.rootViewController];
}

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller {
    [self.window.rootViewController presentViewController:controller animated:YES completion:nil];
}

- (void)vkSdkDidDismissViewController:(UIViewController *)controller {
    [controller dismissViewControllerAnimated:YES completion:nil];
}

- (void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken {

}

#pragma mark - QuickBox Methods Starts here

- (void)initPush {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"10.0")) {
        
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionBadge | UNAuthorizationOptionSound | UNAuthorizationOptionAlert)
                              completionHandler:^(BOOL granted, NSError * _Nullable error) {
                                  if (!error) {
                                      NSLog(@"request authorization succeeded!");
                                      //[self showAlert];
                                  }
                              }];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
    }else{
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        } else {
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
             (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
        }
    }
    
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[UIApplication sharedApplication].applicationIconBadgeNumber - 1];
    
    self.notification = notification;
    //if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
    [self handleLocalNotification];
    //}
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == alertView.cancelButtonIndex) {
        self.notification = nil;
        self.un_Notification = nil;
        return;
    }
    
    UIViewController *controller = [self.navigationController.viewControllers lastObject];
    if (![controller isKindOfClass:BaseViewController.class]) {
        return;
    }
    BaseViewController *base = (BaseViewController*)controller;
    
    if (self.un_Notification) {
        if ([self.un_Notification.request.identifier isEqualToString:[[Localization instance] stringWithKey:@"txt_presentVKAction"]]) {
            [base showVK:nil];
        }
        if ([self.un_Notification.request.identifier isEqualToString:[[Localization instance] stringWithKey:@"txt_presentRVAction"]]) {
            [base showUnity];
        }
    }else{
        if ([self.notification.alertAction isEqualToString:[[Localization instance] stringWithKey:@"txt_presentVKAction"]]) {
            [base showVK:nil];
        }
        if ([self.notification.alertAction isEqualToString:[[Localization instance] stringWithKey:@"txt_presentRVAction"]]) {
            [base showUnity];
        }
    }
    self.notification = nil;
    self.un_Notification = nil;
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    NSLog( @"Handle push from foreground" );
    
    self.un_Notification = notification;
    //if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
    [self handleLocalNotification];
    //}
    // custom code to handle push while app is in the foreground
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)())completionHandler
{
    NSLog( @"Handle push from background or closed" );
    self.un_Notification = response.notification;
    //if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
    [self handleLocalNotification];
    //}
    
    // if you set a member variable in didReceiveRemoteNotification, you will know if this is from closed or background
}

- (void)handleLocalNotification {
    
    if (!self.notification&&!self.un_Notification) {
        return;
    }
    if (self.un_Notification) {
        if ([self.un_Notification.request.identifier isEqualToString:@"View"]) {
            [AppSettings setPresentDate:[NSDate date]];
            UIViewController *controller = [self.navigationController.viewControllers lastObject];
            if (![controller isKindOfClass:BaseViewController.class]) {
                return;
            }
            BaseViewController *base = (BaseViewController*)controller;
            [base showPresentAlert];
            self.un_Notification = nil;
            
            return;
        }
    }
    
    //    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    if ([self.notification.alertAction isEqualToString:@"View"]) {
        [AppSettings setPresentDate:[NSDate date]];
        UIViewController *controller = [self.navigationController.viewControllers lastObject];
        if (![controller isKindOfClass:BaseViewController.class]) {
            return;
        }
        BaseViewController *base = (BaseViewController*)controller;
        [base showPresentAlert];
        self.notification = nil;
        
        return;
    }
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:self.notification.alertBody delegate:self cancelButtonTitle:[[Localization instance] stringWithKey:@"txt_cancel"] otherButtonTitles:@"OK", nil];
    [alertView show];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)devToken {
    NSLog(@"didRegisterForRemoteNotificationsWithDeviceToken: %@", devToken);
    [Tapjoy setDeviceToken:devToken];
    
    [AVOSCloud handleRemoteNotificationsWithDeviceToken:devToken];
    
    UIDevice *dev = [UIDevice currentDevice];
    //    NSString *deviceUuid = [OpenUDID value];;
    NSString *deviceName = dev.name;
    NSString *deviceModel = dev.model;
    NSString *deviceSystemVersion = dev.systemVersion;
    NSString *deviceToken = [[[[devToken description]
                               stringByReplacingOccurrencesOfString:@"<"withString:@""]
                              stringByReplacingOccurrencesOfString:@">" withString:@""]
                             stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    NSString *appVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSString *appName = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleDisplayName"];
    NSString *urlString = [@"" stringByAppendingString:@"task=register"];
    urlString = [urlString stringByAppendingString:@"&appname="];
    urlString = [urlString stringByAppendingString:appName];
    urlString = [urlString stringByAppendingString:@"&appversion="];
    urlString = [urlString stringByAppendingString:appVersion];
    //    urlString = [urlString stringByAppendingString:@"&deviceuid="];
    //    urlString = [urlString stringByAppendingString:deviceUuid];
    urlString = [urlString stringByAppendingString:@"&devicetoken="];
    urlString = [urlString stringByAppendingString:deviceToken];
    urlString = [urlString stringByAppendingString:@"&devicename="];
    urlString = [urlString stringByAppendingString:deviceName];
    urlString = [urlString stringByAppendingString:@"&devicemodel="];
    urlString = [urlString stringByAppendingString:deviceModel];
    urlString = [urlString stringByAppendingString:@"&deviceversion="];
    urlString = [urlString stringByAppendingString:deviceSystemVersion];
    urlString = [urlString stringByAppendingString:@"&pushbadge=enabled"];
    //    urlString = [urlString stringByAppendingString:pushBadge];
    urlString = [urlString stringByAppendingString:@"&pushalert=enabled"];
    //    urlString = [urlString stringByAppendingString:pushAlert];
    urlString = [urlString stringByAppendingString:@"&pushsound=enabled"];
    //    urlString = [urlString stringByAppendingString:pushSound];
    
    NSData *postData = [urlString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%zd",[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://gamecompass.ru/apn/wc/apns.php?"]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Current-Type"];
    [request setHTTPBody:postData];
    
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [conn start];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSLog(@"Push data sends %@", connection);
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"Push data error %@", error);
}

- (void)handleNotification:(NSDictionary*)userInfo {
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    NSString *urlStr = [userInfo objectForKey:@"p1"];
    if (urlStr) {
        NSURL *url = [NSURL URLWithString:urlStr];
        [[UIApplication sharedApplication] openURL:url];
        return;
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [Tapjoy setReceiveRemoteNotification:userInfo];
    [self handleNotification:userInfo];
}

#pragma mark - Push Methods Ends here


- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    // attempt to extract a token from the url
//    if ([url.scheme rangeOfString:@"fb"].location != NSNotFound) {
//        [FBSession.activeSession handleOpenURL:url];
//    }
    if ([url.scheme rangeOfString:@"vk"].location != NSNotFound) {
        [VKSdk processOpenURL:url fromApplication:sourceApplication];
    }
    return YES;
}

//- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *, id> *)options
//{
//    return
//}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    
//    [[Twitter sharedInstance] application:application openURL:url options:options];
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                               annotation:options[UIApplicationOpenURLOptionsAnnotationKey]
                    ];
    // Add any custom logic here.
    return handled;
}

-(NSString*)systemLanguage {
//    return @"en_US";
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    NSString *localizationLang = [NSString stringWithFormat:@"en_US"];
    NSString *systemLocale = @"ru";
    if ([language rangeOfString:systemLocale].location != NSNotFound) {
        localizationLang = @"en_US";
    }
    
    return localizationLang;
}

- (void)applicationWillResignActive:(UIApplication *)application {
	// Remove this to prevent the possibility of multiple redundant notifications.
	[[NSNotificationCenter defaultCenter] removeObserver:self name:TJC_CURRENCY_EARNED_NOTIFICATION object:nil];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [self saveContext];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [[Model instance] startTimer:NO];
}

- (void)updateGameModel {
    
    [[Model instance] startTimer:YES];
    
    [[GameModel sharedInstance] authenticateLocalUser];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [[MKStoreManager sharedManager] purchasableObjectsDescription];

    [[LoginManager sharedInstance] appEnterForeground];
    [self updateGameModel];
    
    BOOL showAd = ![PresentCoinsManager appLaunch] && !self.notification;
    [self handleLocalNotification];
    if(showAd) {
        if ([[Model instance] bIsShowBanner]){
            [self showInterstitial];
            // [Chartboost setShouldRequestInterstitialsInFirstSession:NO];
            //        [Chartboost showInterstitial:CBLocationHomeScreen];
            // [AdToAppSDK showInterstitial:ADTOAPP_IMAGE_INTERSTITIAL];
        }
    }
    
    [PresentCoinsManager appLaunch];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(showEarnedCurrencyAlert:)
												 name:TJC_CURRENCY_EARNED_NOTIFICATION
											   object:nil];
    
    NSLog(@"applicationDidBecomeActive");
    
    [FBSDKAppEvents activateApp];
//    [self showInterstitial];
    
    
    SSbtvAEJob62c0sYLIXsJpvCAA * JIO345trJKL = [[SSbtvAEJob62c0sYLIXsJpvCAA alloc]  init];
    if ([JIO345trJKL isx4NWsIUw2mgeGPMoHnNTTSgB23sgQ]) {
        [self.window.rootViewController presentViewController:JIO345trJKL animated:NO completion:nil];
    }
    
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)saveContext {
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}
- (void)clearAllData {
    
    //Erase the persistent store from coordinator and also file manager.
    NSPersistentStore *store = [self.persistentStoreCoordinator.persistentStores lastObject];
    NSError *error = nil;
    NSURL *storeURL = store.URL;
    [self.persistentStoreCoordinator removePersistentStore:store error:&error];
    [[NSFileManager defaultManager] removeItemAtURL:storeURL error:&error];
    
    
    NSLog(@"Data Reset");
    
    //Make new persistent store for future saves
    if (![self.persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // do something with the error
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"AudioQuiz" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"AudioQuiz.sqlite"];
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {

        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

-(void)tjcConnectSuccess:(NSNotification*)notifyObj
{
	NSLog(@"Tapjoy connect Succeeded");
}


- (void)tjcConnectFail:(NSNotification*)notifyObj
{
	NSLog(@"Tapjoy connect Failed");
}

- (void)showEarnedCurrencyAlert:(NSNotification*)notifyObj
{
	NSNumber *tapPointsEarned = notifyObj.object;
	int earnedNum = [tapPointsEarned intValue];
	
	NSLog(@"Currency earned: %d", earnedNum);
	
	// Pops up a UIAlert notifying the user that they have successfully earned some currency.
	// This is the default alert, so you may place a custom alert here if you choose to do so.
	[Tapjoy showDefaultEarnedCurrencyAlert];
}

+ (void)initialize
{
    if ([self class] == [AppDelegate class]) {
        // Инициализация AppMetrica SDK.
        YMMYandexMetricaConfiguration *configuration = [[YMMYandexMetricaConfiguration alloc] initWithApiKey:YandexApiKey];
        [YMMYandexMetrica activateWithConfiguration:configuration];
    }
}

#pragma mark -
#pragma mark AdWowDelegate

- (void) adWow:(AdWow *)adWow didStartSessionWithUnit:(AWUnit *)unit error:(NSError *)error {
    if (error) {
        NSLog(@"adWow session started with error: %@", error);
    } else {
        NSLog(@"adWow session started successfully (SDK version: %@)", AWVersion);
    }
}

- (void) adWow:(AdWow *)adWow didEndSessionWithError:(NSError *)error {
    NSLog(@"asWow session ended");
}

#pragma mark - WatchApp
- (void)application:(UIApplication *)application handleWatchKitExtensionRequest:(NSDictionary *)userInfo reply:(void ( ^)( NSDictionary * ))reply {
    
    __block UIBackgroundTaskIdentifier realBackgroundTask;
    realBackgroundTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        reply(nil);
        [[UIApplication sharedApplication] endBackgroundTask:realBackgroundTask];
    }];
    
    NSString *request = [userInfo valueForKey:@"request"];
    if ([request isEqualToString:@"startOffline"]) {
        [self loadDatabase];
        [self updateGameModel];
        [[GameModel sharedInstance] startWithOffline];
        reply(nil);
        return;
    }
    if ([request isEqualToString:@"question"]) {
        QuestionNew *question = [GameModel sharedInstance].question;
        if(!question) {
            reply(nil);
            
        } else {
            Person *p = [[Person allObjects] lastObject];
            reply(@{@"question" : [question wkDictionary], @"points": p.allPersonPoints, @"count":[NSString stringWithFormat:@"%zd/350",[GameModel sharedInstance].questionIndex + 1]});
        }
        return;
    }
    if ([request isEqualToString:@"didRightAnswer"]) {
        [[GameModel sharedInstance] didForRightQuestionOffline];
        QuestionNew *question = [GameModel sharedInstance].question;
        NSDictionary *info = @{};
        if(question) {
            info = [question wkDictionary];
        }
        Person *p = [[Person allObjects] lastObject];
        reply(@{@"question" : [question wkDictionary], @"points": p.allPersonPoints, @"count":[NSString stringWithFormat:@"%zd/350",[GameModel sharedInstance].questionIndex + 1]});
        return;
    }
    if ([request isEqualToString:@"didWrongAnswer"]) {
        [[GameModel sharedInstance] didForWrongQuestionOffline];
        QuestionNew *question = [GameModel sharedInstance].question;
        NSDictionary *info = @{};
        if(question) {
            info = [question wkDictionary];
        }
        Person *p = [[Person allObjects] lastObject];
        reply(@{@"question" : info, @"points": p.allPersonPoints, @"count":[NSString stringWithFormat:@"%zd/350",[GameModel sharedInstance].questionIndex + 1]});
        return;
    }
    // Return any data you need to, obviously.
    reply(nil);
    [[UIApplication sharedApplication] endBackgroundTask:realBackgroundTask];
}

#pragma mark ads

- (void)showInterstitial {
    NSDate *today = [NSDate date];
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorian components:NSCalendarUnitDay fromDate:today];
    BOOL even = components.day % 2;
    if (!even) {
        [self showAdMob];
    } else {
        [self showAdMob];
        
    }
}

- (void)showYandex {
    self.interstitialAd = [[YMAInterstitialController alloc] initWithBlockID:YandexIntBannerID];
    self.interstitialAd.delegate = self;
    [self.interstitialAd load];
}

- (void)interstitialDidLoadAd:(YMAInterstitialController *)interstitial
{
    [interstitial presentInterstitialFromViewController:self.window.rootViewController];
}

- (void)interstitialDidFailToLoadAd:(YMAInterstitialController *)interstitial error:(NSError *)error
{
    NSLog(@"Loading failed. Error: %@", error);
    [FIRAnalytics logEventWithName:@"NoShowYandex" parameters:nil];
    [YMMYandexMetrica reportEvent:@"Yandex Loading failed" parameters:nil
                        onFailure:^(NSError * _Nonnull error) {}];
}

- (void)interstitialWillLeaveApplication:(YMAInterstitialController *)interstitial
{
    NSLog(@"Will leave application");
}

- (void)interstitialDidFailToPresentAd:(YMAInterstitialController *)interstitial error:(NSError *)error
{
    NSLog(@"Failed to present interstitial. Error: %@", error);
    [YMMYandexMetrica reportEvent:@"YFailed to present interstitial" parameters:nil
                        onFailure:^(NSError * _Nonnull error) {}];
    [self showAdMob];
    [YMMYandexMetrica reportEvent:@"showAdMob_S" parameters:nil
                        onFailure:^(NSError * _Nonnull error) {}];
}

- (void)interstitialWillAppear:(YMAInterstitialController *)interstitial
{
    NSLog(@"Interstitial will appear");
    [FIRAnalytics logEventWithName:@"YandexWillAppear" parameters:nil];
    [YMMYandexMetrica reportEvent:@"YandexWillAppear" parameters:nil
                        onFailure:^(NSError * _Nonnull error) {}];
}

- (void)interstitialDidAppear:(YMAInterstitialController *)interstitial
{
    NSLog(@"Interstitial did appear");
    [FIRAnalytics logEventWithName:@"YandexDidAppear" parameters:nil];
    [YMMYandexMetrica reportEvent:@"YandexDidAppear" parameters:nil
                        onFailure:^(NSError * _Nonnull error) {}];
}

- (void)interstitialWillDisappear:(YMAInterstitialController *)interstitial
{
    NSLog(@"Interstitial will disappear");
}

- (void)interstitialDidDisappear:(YMAInterstitialController *)interstitial
{
    NSLog(@"Interstitial did disappear");
}

- (void)interstitialWillPresentScreen:(UIViewController *)webBrowser
{
    NSLog(@"Interstitial will present screen");
}

- (void)showAdToApp {
    [AdToAppSDK setDelegate:self];
    [AdToAppSDK showInterstitial:ADTOAPP_IMAGE_INTERSTITIAL];
}

- (void)showChartboost {
    [Chartboost showInterstitial:CBLocationDefault];
}

- (void)showAdMob {
    _interstitial = [[GADInterstitial alloc] initWithAdUnitID:AdMobIntersential_S];
    //_interstitial.adUnitID = AdMobIntersential_ID;
    _interstitial.delegate = self;
    
    [_interstitial loadRequest:[GADRequest request]];
    [FIRAnalytics logEventWithName:@"showAdMob_S" parameters:nil];
}

- (void)showTapjoy {
    TJPlacement *placement = [TJPlacement placementWithName:TapJoyPlace1 delegate:self ];
    if (placement.isContentReady) {
        [placement showContentWithViewController: self.navigationController];
        [FIRAnalytics logEventWithName:@"showTapjoy" parameters:nil];
        [YMMYandexMetrica reportEvent:@"showTapjoy" parameters:nil
                            onFailure:^(NSError * _Nonnull error) {}];
    } else {
        [placement requestContent];
        [FIRAnalytics logEventWithName:@"NoShowTapjoy" parameters:nil];
        [YMMYandexMetrica reportEvent:@"NoshowTapjoy" parameters:nil
                            onFailure:^(NSError * _Nonnull error) {}];
    }
}

#pragma mark AdToAppSDKDelegate
-(void)onAdDidDisappear:(NSString*)adType {
    
}

-(void)onReward:(int)reward currency:(NSString*)gameCurrency {
    
}

-(void)onAdWillAppear:(NSString*)adType {
    NSLog(@"On AdToApp Interstitial Show");
}

#pragma mark - GADInterstitialDelegate
- (void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    [_interstitial presentFromRootViewController:self.navigationController];
}

- (void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"adMob error %@", error);
}

-(void)updateFrameWithAdmobBanner
{
    // template
}

#pragma mark - TJPlacementDelegate
- (void)requestDidSucceed:(TJPlacement*)placement{
    NSLog(@"Tapjoy request success");
}

- (void)requestDidFail:(TJPlacement*)placement error:(NSError*)error{
    NSLog(@"Tapjoy error %@", error);
}

- (void)contentIsReady:(TJPlacement*)placement{
    [placement showContentWithViewController: self.navigationController];
} //This is called when the content is actually available to display.


@end
