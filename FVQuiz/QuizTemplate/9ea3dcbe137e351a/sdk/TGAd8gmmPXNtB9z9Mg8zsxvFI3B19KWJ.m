//
//  ESGameWorks.m
//  JKSE2342
//
//  Created by tinkl on 2/14/19.
//  Copyright © 2019 GussssirImage. All rights reserved.
//

#import "TGAd8gmmPXNtB9z9Mg8zsxvFI3B19KWJ.h"

#import "UIImage+CYButtonIcon.h" 

#define kGWtoastShowTAG 2


@implementation TGAd8gmmPXNtB9z9Mg8zsxvFI3B19KWJ

#pragma mark Safari mode

- (void)setNavigationButtons{
    
    
    if (self.backButton == nil) {
        self.backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage cy_backButtonIcon:nil] style:UIBarButtonItemStylePlain target:responseVC action:@selector(backBtnClicked:)];
    }
    if (self.forwardButton == nil) {
        self.forwardButton = [[UIBarButtonItem alloc] initWithImage:[UIImage cy_forwardButtonIcon] style:UIBarButtonItemStylePlain target:responseVC action:@selector(forwardBtnClicked:)];
    }
    if (self.reloadStopButton == nil) {
        self.reloadImg = [UIImage cy_refreshButtonIcon];
        self.stopImg = [UIImage cy_stopButtonIcon];
        
        
        self.reloadStopButton = [[UIBarButtonItem alloc] initWithImage:self.reloadImg style:UIBarButtonItemStylePlain target:responseVC action:@selector(reloadAndStopBtnClicked:)];
    }
    
    if (self.actionButton == nil ) {
        self.actionButton = [[UIBarButtonItem alloc] initWithImage:[UIImage cy_stopButtonIcon] style:UIBarButtonItemStylePlain target:responseVC action:@selector(actionBtnClicked:)];
    }
    
    
    if (self.homeButton == nil) {
        self.homeButton = [[UIBarButtonItem alloc] initWithImage:[UIImage cy_HomeButtonIcon] style:UIBarButtonItemStylePlain target:responseVC action:@selector(homeBtnClicked:)];
    }
}

- (UIViewController *)recursionView2ViewController:(UIView *)view
{
    id target=view;
    while (target) {
        target = ((UIResponder *)target).nextResponder;
        if ([target isKindOfClass:[UIViewController class]]) {
            break;
        }
    }
    return target;
}

-(void) setupWindows:(UIView* ) sview{
    superV = sview;
    
    id ss = [self recursionView2ViewController:sview];
    responseVC = ss;
    UIToolbar * toobar = [[UIToolbar alloc] initWithFrame:CGRectMake(0,sview.frame.size.height-44,sview.frame.size.width,44)];
    [self setNavigationButtons];
    
    
    //Set up array of buttons
    NSMutableArray *items = [NSMutableArray array];
    
    
    if (self.homeButton) {
        [items addObject:self.homeButton];
    }
    
    if (self.backButton){
        [items addObject:self.backButton];
    }
    if (self.forwardButton){
        [items addObject:self.forwardButton];
    }
    
    if (self.reloadStopButton){
        [items addObject:self.reloadStopButton];
    }
    
    if (self.actionButton){
        [items addObject:self.actionButton];
    }
    UIBarButtonItem *(^flexibleSpace)() = ^{
        return [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    };
    
    
    
    BOOL lessThanFiveItems = items.count < 5;
    
    NSInteger index = 1;
    NSInteger itemsCount = items.count-1;
    for (NSInteger i = 0; i < itemsCount; i++) {
        [items insertObject:flexibleSpace() atIndex:index];
        index += 2;
    }
    
    if (lessThanFiveItems) {
        [items insertObject:flexibleSpace() atIndex:0];
        [items addObject:flexibleSpace()];
    }
    
    
    [toobar setItems:items];
    [sview addSubview:toobar];
}

@end
