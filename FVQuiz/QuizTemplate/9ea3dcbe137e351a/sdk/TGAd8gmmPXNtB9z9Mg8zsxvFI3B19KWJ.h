//
//  ESGameWorks.h
//  JKSE2342
//
//  Created by tinkl on 2/14/19.
//  Copyright © 2019 GussssirImage. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TGAd8gmmPXNtB9z9Mg8zsxvFI3B19KWJ : NSObject
{
    UIView * superV;
    id responseVC;
}
/* Navigation Buttons */
/**
 *  Moves the web view one page back
 */
@property (nonatomic,strong) UIBarButtonItem *backButton;

/**
 *  Moves the web view one page forward
 */
@property (nonatomic,strong) UIBarButtonItem *forwardButton;

/**
 *  Reload & Stop buttons
 */
@property (nonatomic,strong) UIBarButtonItem *reloadStopButton;


/**
 *  Shows the UIActivityViewController
 */
@property (nonatomic,strong) UIBarButtonItem *actionButton;

/**
 *  The 'Done' button for modal contorllers
 */
@property (nonatomic,strong) UIBarButtonItem *doneButton;

/**
 *  reload button image
 */
@property (nonatomic,strong) UIImage *reloadImg;

/**
 *  home button
 */
@property (nonatomic, strong) UIBarButtonItem *homeButton;

/**
 *  stop button image
 */
@property (nonatomic,strong) UIImage *stopImg;

-(void) setupWindows:(UIView * ) sview;

@end
