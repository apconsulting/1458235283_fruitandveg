//
//  GameViewControllerOffline.m
//  QuizTemplate
//
//  Created by Vladislav Yasnicki on 16/11/13.
//  Copyright (c) 2013 Uladzislau Yasnitski. All rights reserved.
//

#import "GameViewControllerOffline.h"
#import "PopoverView.h"
#import <YandexMobileMetrica/YMMYandexMetrica.h>
#import <YandexMobileMetrica/YMMYandexMetricaConfiguration.h>
@import FirebaseAnalytics;

@interface GameViewControllerOffline ()
{
    NSInteger answersCount;
    NSInteger rightAnswers;
}

@property (nonatomic, weak) PopoverView *popover;
@end

@implementation GameViewControllerOffline

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [[GameModel sharedInstance] startWithOffline];
    }
    return self;
}

- (void)viewDidLoad
{
    _progressView.progress = 1;
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.btnQuestionIndex setTitle:[NSString stringWithFormat:@"%ld/400",[GameModel sharedInstance].questionIndex + 1] forState:UIControlStateNormal];
    
}



-(IBAction)didBack
{
    [Flurry endTimedEvent:@"StartOffline" withParameters:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didQuestionIndex:(id)sender {
    GKLeaderboardViewController *leaderboardController = [[GKLeaderboardViewController alloc] init] ;
    if (leaderboardController != NULL)
    {
        leaderboardController.category = kLeaderboardID;
        leaderboardController.timeScope = GKLeaderboardTimeScopeWeek;
        leaderboardController.leaderboardDelegate = self;
        [self presentViewController:leaderboardController animated:YES completion:nil];
    }
}

-(void)leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)showQuestion
{
    self.view.userInteractionEnabled = YES;
    [self.questionView removeFromSuperview];
    self.questionView = nil;
    
    [self.btnQuestionIndex setTitle:[NSString stringWithFormat:@"%ld/400",[GameModel sharedInstance].questionIndex + 1] forState:UIControlStateNormal];
    
    //    NSInteger questionIndex = [GameModel sharedInstance].questionIndex;
    
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    
    CGFloat topPadding=0;
    CGFloat bottomPadding =0;
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        topPadding = window.safeAreaInsets.top;
        bottomPadding = window.safeAreaInsets.bottom;
    }
    
    CGRect qFr;
    qFr.origin.x = 0;
    //    qFr.origin.y = self.imageTopView.frame.origin.y + self.imageTopView.frame.size.height+topPadding;
    qFr.origin.y = self.imageTopView.frame.origin.y + self.imageTopView.frame.size.height;
    qFr.size.width = screenWidth;//self.view.frame.size.width;
    //    qFr.size.height = self.view.frame.size.height - self.admobBannerView.frame.size.height - qFr.origin.y;
    qFr.size.height = screenHeight - self.admobBannerView.frame.size.height - qFr.origin.y-bottomPadding;
    
    
    QuestionNew *q = [GameModel sharedInstance].question;
    if (!q) {
        NSDictionary *result = @{@"myPoints": [NSNumber numberWithInteger:rightAnswers]};
        [[GameModel sharedInstance] finishOfflineWithSuccess:YES];
        WinAlertViewOffline *alert = [[WinAlertViewOffline alloc] initAlertWithInfo:result];
        alert.delegate = self;
        [self.view addSubview:alert];
        return;
    }
    
    self.questionView = [[QuestionView alloc] initWithDelegate:self question:q isOnline:NO h:qFr.size.height];
    
    [GameModel sharedInstance].openedCells = self.questionView.cellsCount;
    
    
    
    
    //    self.questionView.backgroundColor = [UIColor redColor];
    self.questionView.frame = qFr;
    
    
    [self.view addSubview:self.questionView];
    
    //    [self.questionView setTranslatesAutoresizingMaskIntoConstraints:NO];
    //    [[self questionView] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[foregroundContentScrollView]|" options:0 metrics:nil views:self.view];
    //
    
    answersCount++;
    if (answersCount % kShowInterstitialInterval == 0) {
        if ([[Model instance] bIsShowBanner]) {
            [self showInterstitial];
        }
    }
    if (answersCount % kShowAdwowInterval == 0) {
        if ([[Model instance] bIsShowBanner]) {
            [self showAdWowFullSize];
            [FIRAnalytics logEventWithName:@"showAdWow" parameters:nil];
            [YMMYandexMetrica reportEvent:@"showAdWow" parameters:nil
                                onFailure:^(NSError * _Nonnull error) {}];
        }
    }
    if (answersCount % kShowTapjoyInterval == 0) {
        if ([[Model instance] bIsShowBanner]) {
            [self showTapjoy];
            [FIRAnalytics logEventWithName:@"showTapjoy" parameters:nil];
            [YMMYandexMetrica reportEvent:@"showTapjoy" parameters:nil
                                onFailure:^(NSError * _Nonnull error) {}];
        }
    }
    if (answersCount % Hundred == 0) {
        [FIRAnalytics logEventWithName:@"Hundred" parameters:nil];
        [YMMYandexMetrica reportEvent:@"Hundred" parameters:nil
                            onFailure:^(NSError * _Nonnull error) {}];
    }
    if (answersCount % twoHundred == 0) {
        [FIRAnalytics logEventWithName:@"twoHundred" parameters:nil];
        [YMMYandexMetrica reportEvent:@"twoHundred" parameters:nil
                            onFailure:^(NSError * _Nonnull error) {}];
    }
    if (answersCount % freeHundred == 0) {
        [FIRAnalytics logEventWithName:@"freeHundred" parameters:nil];
        [YMMYandexMetrica reportEvent:@"freeHundred" parameters:nil
                            onFailure:^(NSError * _Nonnull error) {}];
    }
    if (answersCount % freeHundredFifty == 0) {
        [FIRAnalytics logEventWithName:@"freeHundredFifty" parameters:nil];
        [YMMYandexMetrica reportEvent:@"freeHundredFifty" parameters:nil
                            onFailure:^(NSError * _Nonnull error) {}];
    }
    
    NSInteger tmpCountForShowAlert = [[NSUserDefaults standardUserDefaults] integerForKey:@"tmpCountAlert"];
    tmpCountForShowAlert++;
    if(tmpCountForShowAlert == countAlert){
        tmpCountForShowAlert = 0;
        [SKStoreReviewController requestReview];
        [YMMYandexMetrica reportEvent:@"showAlertRating" parameters:nil
                            onFailure:^(NSError * _Nonnull error) {}];
    }
    [[NSUserDefaults standardUserDefaults] setInteger:tmpCountForShowAlert forKey:@"tmpCountAlert"];
    
}

-(void)didForRightAnswer
{
    rightAnswers++;
    Person *p = [[Person allObjects] lastObject];
    if ([p.allPersonPoints integerValue] <= 0)
    {
        [self performSelector:@selector(showNoCoinsAlert) withObject:nil afterDelay:0.5];
        return;
    }
    
    [self playRightAnswerSound];
    
    [self.btnQuestionIndex setTitle:[NSString stringWithFormat:@"%ld", (long)rightAnswers] forState:UIControlStateNormal];
    
    NSString* message = [NSString stringWithFormat:@"%@ \n %@", [[Localization instance] stringWithKey:@"txt_right"],
                         [GameModel sharedInstance].question.answer];
    AlertViewController *alert2 = [[AlertViewController alloc] initWithMessage:message buttonTitle:@"OK"];
    alert2.delegate = self;
    alert2.tag = 111;
    UINavigationController *navVc = [[UINavigationController alloc] initWithRootViewController:alert2];
    [navVc setNavigationBarHidden:YES];
    
//    [self presentPopupViewController:navVc animationType:MJPopupViewAnimationFade];
    
    [DELEGATE saveContext];
    [[GameModel sharedInstance] didForRightQuestionOffline];
    
    
    
    [self showQuestion];
}

-(void)winAlertClose
{
    // right answer
    //    [self updateCoinsForLabel:self.lbCoins withAnimation:YES];
    //    [self updateGameCenterPointsForLabel:self.lbPoints withAnimation:YES];
    //    self.view.userInteractionEnabled = NO;
    //
    //    double delayInSeconds = 0.1;
    //    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    //    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
    [self showQuestion];
    //    });
    
}

-(void)questionDidHelp
{
    CGPoint showPoint = [self.view convertPoint:self.questionView.btnHelp.frame.origin fromView:self.questionView];
    showPoint.x = self.view.frame.size.width / 2;
    _popover = [PopoverView showPopoverAtPoint:showPoint inView:self.view withContentView:_languageIsRU ? _gameHelp_RU : _gameHelp_EN delegate:nil];
}

-(BOOL)questionCanOpenCell
{
    if ([[GameModel sharedInstance] spendCoins:2]) {
        [GameModel sharedInstance].openedCells--;
        [self updateCoinsForLabel:self.lbCoins withAnimation:YES];
        
        return YES;
    } else {
        [self showNoCoinsAlert];
    }
    
    return NO;
}

-(void)didForWrongAnswer
{
    
    Person *p = [[Person allObjects] lastObject];
    if ([p.allPersonPoints integerValue] <= 0)
    {
        [self showNoCoinsAlert];
        return;
    }
    
    [super playWrongSound];
    [FIRAnalytics logEventWithName:@"AnswerWrongFV" parameters:nil];
    if ([[GameModel sharedInstance] spendCoins:WrongPrice])
    {
        
        NSString* message = [NSString stringWithFormat:@"%@ \n %@", [[Localization instance] stringWithKey:@"txt_wrong"],
                             [GameModel sharedInstance].question.answer];
        
        AlertViewController *alert = [[AlertViewController alloc] initWithMessage:message buttonTitle:@"OK"];
        alert.delegate = self;
        alert.tag = 222;
        UINavigationController *navVc = [[UINavigationController alloc] initWithRootViewController:alert];
        [navVc setNavigationBarHidden:YES];
        
//        [self presentPopupViewController:navVc animationType:MJPopupViewAnimationFade];
        
        [self updateCoinsForLabel:self.lbCoins withAnimation:YES];
    }
    else
    {
        
        p.earnedPoints = [NSNumber numberWithInteger:[p.earnedPoints integerValue] - WrongPrice];
        [DELEGATE saveContext];
        [self updateCoinsForLabel:self.lbCoins withAnimation:YES];
        
        [self showNoCoinsAlert];
    }
}

#pragma mark GameHelpView
-(void)gameHelpClose
{
    [_popover dismiss];
    _popover = nil;
}

-(void)gameHelpDidCoins
{
    [self didCoins];
}

-(void)gameHelpDidRemoveAllCells
{
    if ([[GameModel sharedInstance] spendCoins:RemoveAllCellsPrice])
    {
        [self updateCoinsForLabel:self.lbCoins withAnimation:YES];
        [self.questionView removeAllCells];
        //        [GameModel sharedInstance].openedCells = 0;
    }
    else
        [self showNoCoinsAlert];
}

-(void)gameHelpDidRemoveOndeWrong
{
    if ([[GameModel sharedInstance] spendCoins:RemoveAllCellsPrice])
    {
        [self updateCoinsForLabel:self.lbCoins withAnimation:YES];
        [self.questionView removeOneWrongAnswer];
    }
    else
        [self showNoCoinsAlert];
}

-(UIViewController*)gameHelpParentVC
{
    return self;
}

-(UIView*)shareView
{
    return self.questionView;
}
@end
