//
//  QuestionView.m
//  QuizTemplate
//
//  Created by Uladzislau Yasnitski on 11/12/13.
//  Copyright (c) 2013 Uladzislau Yasnitski. All rights reserved.
//

#import "QuestionView.h"
#import "UIColor+NewColor.h"
#import "GeneralUtils.h"
#import "UIFont+CustomFont.h" 
#import "OpenedCell.h"
#import "AppSettings.h"
#import "NSMutableArray+Shuffling.h"
#import "const.h"

@interface QuestionView ()

@property (nonatomic, strong) QuestionNew *question;
@property (nonatomic, strong) NSDictionary *answers;
@property (nonatomic, retain) NSMutableDictionary *buttons;
//@property (nonatomic, retain) NSMutableDictionary *labels;

//@property (nonatomic, strong) FonQuestion *fonQuestion;
@end

@implementation QuestionView

- (id)initWithDelegate:(id<QuestionViewDelegate>)delegate question:(QuestionNew *)question isOnline:(BOOL)isOnline h:(CGFloat)newH
{
    if (isPad)
    {
        [[NSBundle mainBundle] loadNibNamed:isOnline ? @"QuestionViewOnline_iPad" : @"QuestionViewOffline_iPad" owner:self options:nil];
    }
    else
    {
        if (isPhone)
            [[NSBundle mainBundle] loadNibNamed:isOnline ? @"QuestionViewOnline_iPhone_4" : @"QuestionViewOffline_iPhone_4" owner:self options:nil];
        else
            [[NSBundle mainBundle] loadNibNamed:isOnline ? @"QuestionViewOnline_iPhone_5" : @"QuestionViewOffline_iPhone_5" owner:self options:nil];
    }
    
    self = [super initWithFrame:CGRectZero];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        _delegate = delegate;
        _question = question;
//        [_view setTranslatesAutoresizingMaskIntoConstraints:NO];
//        [self setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        UIView *subView=_view;
        UIView *parent=self;
        
        subView.translatesAutoresizingMaskIntoConstraints = NO;
        
        //Trailing
        NSLayoutConstraint *trailing =[NSLayoutConstraint
                                       constraintWithItem:subView
                                       attribute:NSLayoutAttributeTrailing
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:parent
                                       attribute:NSLayoutAttributeTrailing
                                       multiplier:1.0f
                                       constant:0.f];
        
        //Leading
        
        NSLayoutConstraint *leading = [NSLayoutConstraint
                                       constraintWithItem:subView
                                       attribute:NSLayoutAttributeLeading
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:parent
                                       attribute:NSLayoutAttributeLeading
                                       multiplier:1.0f
                                       constant:0.f];
        
        //Bottom
        NSLayoutConstraint *top =[NSLayoutConstraint
                                     constraintWithItem:subView
                                     attribute:NSLayoutAttributeTop
                                     relatedBy:NSLayoutRelationEqual
                                     toItem:parent
                                     attribute:NSLayoutAttributeTop
                                     multiplier:1.0f
                                     constant:0.f];
        
//        //Height to be fixed for SubView same as AdHeight
        NSLayoutConstraint *height = [NSLayoutConstraint
                                      constraintWithItem:subView
                                      attribute:NSLayoutAttributeHeight
                                     relatedBy:NSLayoutRelationEqual
                                      toItem:nil
                                      attribute:NSLayoutAttributeNotAnAttribute
                                      multiplier:0
                                      constant:newH];
        
        //Add constraints to the Parent
        [parent addConstraint:trailing];
        [parent addConstraint:top];
        [parent addConstraint:leading];
        
        //Add height constraint to the subview, as subview owns it.
        [subView addConstraint:height];
        
        [self addSubview:_view];
        
        if (!isOnline && ![_question.questionRemoveAllCells boolValue])
        {
            
            NSMutableArray *openedIndexes = [[NSMutableArray alloc] init];
            for (OpenedCell *cell in [[_question openedCells] allObjects])
            {
                NSIndexPath *ip = [NSIndexPath indexPathForRow:[cell.cellY integerValue] inSection:[cell.cellX integerValue]];
                [openedIndexes addObject:ip];
            }
            
//            _fonQuestion = [[FonQuestion alloc] initWithFrame:_imageQuestion.frame openedIndexes:openedIndexes];
//            _fonQuestion.delegate = self;
//            [_view addSubview:_fonQuestion];
        }
        
        
        
        _answers = [NSDictionary dictionaryWithObjects:[_question allQuestionInfos] forKeys:[NSArray arrayWithObjects:[NSNumber numberWithInt:0],[NSNumber numberWithInt:1],[NSNumber numberWithInt:2],[NSNumber numberWithInt:3], nil]];

        _btnFirst.tag = 0;
        _btnSecond.tag = 1;
        _btnThird.tag = 2;
        _btnFourth.tag = 3;
        
        _btnFirst.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _btnSecond.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _btnThird.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _btnFourth.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        
        _btnFirst.titleLabel.textAlignment = NSTextAlignmentCenter;
        _btnSecond.titleLabel.textAlignment = NSTextAlignmentCenter;
        _btnThird.titleLabel.textAlignment = NSTextAlignmentCenter;
        _btnFourth.titleLabel.textAlignment = NSTextAlignmentCenter;
        
        
        
        _buttons = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_btnFirst, [NSNumber numberWithInt:0], _btnSecond,[NSNumber numberWithInt:1], _btnThird, [NSNumber numberWithInt:2], _btnFourth, [NSNumber numberWithInt:3], nil];

        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:_buttons];
        NSMutableDictionary *array = [NSMutableDictionary dictionaryWithDictionary:_answers];
        
        
        NSMutableArray *colors = [[NSMutableArray alloc] initWithObjects:[UIColor colorWith8BitRed:115 green:121 blue:153 alpha:1],[UIColor colorWith8BitRed:219 green:87 blue:49 alpha:1], [UIColor colorWith8BitRed:0 green:166 blue:165 alpha:1], [UIColor colorWith8BitRed:6 green:179 blue:87 alpha:1], nil];
        [colors shuffle];
        
        CGFloat fontSize = isPad ? 28 : 17;
        if(isPhone){
            fontSize = 17;
        }
        
        [dic enumerateKeysAndObjectsUsingBlock:^(NSNumber *key, id obj, BOOL *stop) {
            UIButton *btn = [dic objectForKey:key];
            QustionInfo *answer = [_answers objectForKey:key];
            
            if ([answer.bIsRemoved boolValue])
            {
                UIButton *btn = [_buttons objectForKey:key];
//                [btn removeFromSuperview];
                btn.hidden = YES;
                [_buttons removeObjectForKey:key];
                [array removeObjectForKey:key];
            }
            
            [btn setTitle:answer.title forState:UIControlStateNormal];
            [btn setBackgroundColor:[colors objectAtIndex:[key integerValue]]];
            //            if ([answer.bIsRightAnswer boolValue])
            //                [btn setBackgroundColor:[UIColor greenColor]];
            
            [btn.titleLabel setFont:[UIFont myFontSize:fontSize]];
            [btn.titleLabel setNumberOfLines:0];
            [btn.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
            btn.layer.cornerRadius = 4;
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        }];
        
        _answers = array;
        
        _imageQuestion.image = [UIImage imageWithContentsOfFile:_question.questionImagePath];
        
        NSString *btnImageName;
        if (isPad)
        {
            btnImageName = [NSString stringWithFormat:@"help-ipad_%@.png",[AppSettings currentLanguage]];
        }
        else
        {
            btnImageName = [NSString stringWithFormat:@"help-iphone_%@.png",[AppSettings currentLanguage]];
        }
        [_btnHelp setImage:[UIImage imageNamed:btnImageName] forState:UIControlStateNormal];
        
////            [GeneralUtils roundCornersForImageView:_imageQuestion withCornerRadius:5.0
////                                   andShadowOffset:8.0];

        _imageQuestion.layer.masksToBounds = NO;
        _imageQuestion.layer.cornerRadius = 5; // if you like rounded corners
        _imageQuestion.layer.shadowOffset = CGSizeMake(8.0, 8.0);
        _imageQuestion.layer.shadowRadius = 3;
        _imageQuestion.layer.shadowOpacity = 0.8;
        
        
        UIFont * customFont = [UIFont myFontSize:isPad ? fSizePadQuestionLbl : fSizePhoneQuestionLbl];
        
        CGRect newFrame = _imageQuestion.superview.frame;// CGRectMake(_imageQuestion.frame.origin.x+(isPad?20:10), _imageQuestion.frame.origin.y, _imageQuestion.frame.size.width-2*(isPad?20:10), _imageQuestion.frame.size.height);
        
//        if(isPhone){
//            newFrame = CGRectMake(_imageQuestion.frame.origin.x+(isPad?20:10), _imageQuestion.frame.origin.y-30, _imageQuestion.frame.size.width-2*(isPad?20:10), _imageQuestion.frame.size.height);
//        }
        UILabel *fromLabel = [[UILabel alloc]initWithFrame:newFrame];
        
        fromLabel.text = _question.questionText;
        fromLabel.font = customFont;
        fromLabel.numberOfLines = 0;
        //        fromLabel.lineBreakMode = NSLineBreakByWordWrapping;
        //        fromLabel.numberOfLines = 1;
        //        fromLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines; // or UIBaselineAdjustmentAlignCenters, or UIBaselineAdjustmentNone
        //        fromLabel.adjustsFontSizeToFitWidth = YES;
        //        fromLabel.adjustsLetterSpacingToFitWidth = YES;
        //        fromLabel.minimumScaleFactor = 10.0f/12.0f;
        //        fromLabel.clipsToBounds = YES;
        fromLabel.backgroundColor = [UIColor clearColor];
        fromLabel.textColor = [UIColor blackColor];
        fromLabel.textAlignment = NSTextAlignmentCenter;
        
        
        subView=fromLabel;
        parent=self;
        
        subView.translatesAutoresizingMaskIntoConstraints = NO;
        
        //Trailing
        trailing =[NSLayoutConstraint
                                       constraintWithItem:subView
                                       attribute:NSLayoutAttributeTrailing
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:parent
                                       attribute:NSLayoutAttributeTrailing
                                       multiplier:1.0f
                                       constant:0.f];
        
        //Leading
        
        leading = [NSLayoutConstraint
                                       constraintWithItem:subView
                                       attribute:NSLayoutAttributeLeading
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:parent
                                       attribute:NSLayoutAttributeLeading
                                       multiplier:1.0f
                                       constant:0.f];
        
        //Bottom
        top =[NSLayoutConstraint
                                  constraintWithItem:subView
                                  attribute:NSLayoutAttributeTop
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:parent
                                  attribute:NSLayoutAttributeTop
                                  multiplier:1.0f
                                  constant:0.f];
        
        //        //Height to be fixed for SubView same as AdHeight
        height = [NSLayoutConstraint
                                      constraintWithItem:subView
                                      attribute:NSLayoutAttributeHeight
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:nil
                                      attribute:NSLayoutAttributeNotAnAttribute
                                      multiplier:0
                                      constant:newH/3*2];
        
        //Add constraints to the Parent
        [parent addConstraint:trailing];
        [parent addConstraint:top];
        [parent addConstraint:leading];
        
        //Add height constraint to the subview, as subview owns it.
        [subView addConstraint:height];
        
        [self.view addSubview:fromLabel];
        
        
        [_imageQuestion removeFromSuperview];
    }
    return self;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

//-(void)reloadView
//{
//   
//}


-(void)removeOneWrongAnswer
{
    BOOL needElse = YES;
    
    NSMutableDictionary *array = [NSMutableDictionary dictionaryWithDictionary:_answers];
    
    while (needElse) {
        int index = arc4random() % 4;
        QustionInfo *answer = [array objectForKey:[NSNumber numberWithInt:index]];
        if (answer && ![answer.bIsRightAnswer boolValue])
        {
            [array removeObjectForKey:[NSNumber numberWithInt:index]];
            UIButton *btn = [_buttons objectForKey:[NSNumber numberWithInt:index]];
            btn.hidden=YES;
//            [btn removeFromSuperview];
            [_buttons removeObjectForKey:[NSNumber numberWithInt:index]];
            
            answer.bIsRemoved = [NSNumber numberWithBool:YES];
//            UILabel *lb = [_labels objectForKey:[NSNumber numberWithInt:index]];
//            [lb removeFromSuperview];
//            [_labels removeObjectForKey:[NSNumber numberWithInt:index]];

            needElse = NO;
        }
    }
    
    _answers = array;
    [DELEGATE saveContext];
    
}
-(NSInteger)cellsCount
{
    return 0;
//    return [_fonQuestion cellsCount];
}
-(void)removeAllCells
{
//    SystemSoundID toneSSID = 1104;
//    AudioServicesPlaySystemSound(toneSSID);
//    [_fonQuestion removeAllCellsComplection:^{
//        
//        _question.questionRemoveAllCells = [NSNumber numberWithBool:YES];
//        [DELEGATE saveContext];
//        [_fonQuestion removeFromSuperview];
//        _fonQuestion = nil;
//    }];
}

- (IBAction)didHelp:(id)sender {
    [_delegate questionDidHelp];
}

- (IBAction)didAnswer:(id)sender {
    [_delegate questionDidAnswerQuestion:_question withAnswer:[_answers objectForKey:[NSNumber numberWithInt:[sender tag]]]];
}

-(BOOL)fonQuestionCanOpenVopr:(Cell *)cell
{
    if ([_delegate questionCanOpenCell])
    {
        OpenedCell *openCell = (OpenedCell*)[OpenedCell createObject];
        openCell.cellX = [NSNumber numberWithInt:cell.cellIndexPath.section];
        openCell.cellY = [NSNumber numberWithInt:cell.cellIndexPath.row];
        openCell.question = _question;
        [DELEGATE saveContext];
        return YES;
    }
    return NO;
}
@end
