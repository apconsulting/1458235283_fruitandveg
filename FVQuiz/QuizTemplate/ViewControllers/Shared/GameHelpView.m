//
//  GameHelpViewController.m
//  QuizTemplate
//
//  Created by Uladzislau Yasnitski on 13/11/13.
//  Copyright (c) 2013 Uladzislau Yasnitski. All rights reserved.
//

#import "GameHelpView.h"
#import "AppSettings.h"
#import "CRFileUtils.h"
//#import <FacebookSDK/FacebookSDK.h>
#import "MBProgressHUD.h"
#import "Localization.h"
#import "Flurry.h"
#import "ConstantsAndMacros.h"
#import <ShareKit.h>
#import <SharersCommonHeaders.h>
#import "GameModel.h"
#import "VKSdk.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <YandexMobileMetrica/YMMYandexMetrica.h>
#import <YandexMobileMetrica/YMMYandexMetricaConfiguration.h>

@import FirebaseAnalytics;

@interface GameHelpView () {

}

@end

@implementation GameHelpView


-(id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        
    }
    return self;
}

- (IBAction)didVk:(id)sender {
    [Flurry logEvent:@"DidVkontakte"];
    [FIRAnalytics logEventWithName:@"VK_Help" parameters:nil];
    [YMMYandexMetrica reportEvent:@"VK_Help" parameters:nil
                        onFailure:^(NSError * _Nonnull error) {}];
    CGRect rect = [[_delegate shareView] bounds];
    UIGraphicsBeginImageContextWithOptions(rect.size,YES,0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [[_delegate shareView].layer renderInContext:context];
    UIImage *capturedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    NSString  *imagePath = [CRFileUtils documentPath:@"tempImage.jpg"];
    [UIImageJPEGRepresentation(capturedImage, 0.95) writeToFile:imagePath atomically:YES];
    VKUploadImage *image = [VKUploadImage uploadImageWithImage:[UIImage imageWithContentsOfFile:imagePath] andParams:VKImageTypeJpg];

    VKShareDialogController * shareDialog = [VKShareDialogController new]; //1
    shareDialog.text         = [NSString stringWithFormat:@"%@\n%@\n%@",[[Localization instance] stringWithKey:@"txt_appName"], iTunesLinkRu, [[Localization instance] stringWithKey:@"txt_shareTitle"]]; //2
    shareDialog.uploadImages = @[image];
    shareDialog.shareLink    = [[VKShareLink alloc] initWithTitle:shareLinkTextToVK link:[NSURL URLWithString:iTunesLinkRu]]; //4
    [shareDialog setCompletionHandler:^(VKShareDialogController *dialog, VKShareDialogControllerResult result) {
        if (result == VKShareDialogControllerResultDone) {
            [[GameModel sharedInstance] earnCoins:vkShareErnedCoins];
        }
        [self.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
    }];
    [self.window.rootViewController presentViewController:shareDialog animated:YES completion:nil];
}


- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results {
    NSLog(@"completed");
    __block UIViewController* vc = [_delegate gameHelpParentVC];
    [MBProgressHUD hideHUDForView:vc.view animated:YES];
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error {
    NSLog(@"fail %@",error.description);
    __block UIViewController* vc = [_delegate gameHelpParentVC];
    [MBProgressHUD hideHUDForView:vc.view animated:YES];
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer {
    NSLog(@"cancel");
    __block UIViewController* vc = [_delegate gameHelpParentVC];
    [MBProgressHUD hideHUDForView:vc.view animated:YES];
}


- (IBAction)didFB:(id)sender {

    [Flurry logEvent:@"DidFacebook"];
    [FIRAnalytics logEventWithName:@"FB_Help" parameters:nil];
    [YMMYandexMetrica reportEvent:@"FB_Help" parameters:nil
                        onFailure:^(NSError * _Nonnull error) {}];
    CGRect rect = [[_delegate shareView] bounds];
    UIGraphicsBeginImageContextWithOptions(rect.size,YES,0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [[_delegate shareView].layer renderInContext:context];
    UIImage *capturedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSString  *imagePath = [CRFileUtils documentPath:@"tempImage.jpg"];
   
    __block UIViewController* vc = [_delegate gameHelpParentVC];
    
    
    CGFloat compression = 0.65f;
    CGFloat maxCompression = 0.4f;
    int maxFileSize = 5*1024;
    
    NSData *imageData = UIImageJPEGRepresentation(capturedImage, compression);
    
    while ([imageData length] > maxFileSize && compression > maxCompression)
    {
        compression -= 0.1;
        imageData = UIImageJPEGRepresentation(capturedImage, compression);
    }
    
     [imageData writeToFile:imagePath atomically:YES];
    
    [MBProgressHUD showHUDAddedTo:vc.view animated:YES];
    
    
    /*
     
     */
    
//    FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
//    photo.image = [UIImage imageWithContentsOfFile:imagePath];
//    photo.userGenerated = YES;
//    photo.caption = [NSString stringWithFormat:@"%@\n%@",[[Localization instance] stringWithKey:@"txt_appName"], [[Localization instance] stringWithKey:@"txt_shareTitle"]];
//
//    FBSDKSharePhotoContent *content = [[FBSDKSharePhotoContent alloc] init];
//    content.photos = @[photo];
////    content.contentURL =[NSURL URLWithString:iTunesLink];
//
//    [FBSDKShareDialog showFromViewController:vc
//                                 withContent:content
//                                    delegate:nil];
//
        FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
    photo.image = [UIImage imageWithContentsOfFile:imagePath];//[UIImage imageNamed:imagePath];
    //photo.image = [UIImage imageNamed:shareImageName];
    photo.userGenerated = YES;
    
    FBSDKSharePhotoContent *content = [[FBSDKSharePhotoContent alloc] init];
    content.photos = @[photo];
    
    
//    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
//    content.contentURL = [NSURL URLWithString:iTunesLink];
//    //content.quote = @"текст из приложения";
    
    [FBSDKShareDialog showFromViewController:vc
                                 withContent:content
                                    delegate:self];
    
    [_delegate gameHelpClose];
    /*
     
     */
//    DEFacebookComposeViewController *facebookViewComposer = [[DEFacebookComposeViewController alloc] init] ;
//    vc.modalPresentationStyle = UIModalPresentationCurrentContext;
//
//    [facebookViewComposer addImage:[UIImage imageWithContentsOfFile:imagePath]];
//
//    [facebookViewComposer addURL:[NSURL URLWithString:iTunesLink]];
//    [facebookViewComposer setInitialText:[NSString stringWithFormat:@"%@\n%@",[[Localization instance] stringWithKey:@"txt_appName"], [[Localization instance] stringWithKey:@"txt_shareTitle"]]];
//
//    [facebookViewComposer setCompletionHandler:^(DEFacebookComposeViewControllerResult result) {
//        switch (result) {
//            case DEFacebookComposeViewControllerResultCancelled:
//                //   NSLog(@"Facebook Result: Cancelled");
//                break;
//            case DEFacebookComposeViewControllerResultDone:
//            {
//
//            }
//                break;
//        }
//        [MBProgressHUD hideHUDForView:vc.view animated:YES];
//
//        [vc dismissViewControllerAnimated:YES completion:^{
//           [_delegate gameHelpClose];
//        }];
//
//    }];
    
    
//    [vc presentViewController:facebookViewComposer animated:YES completion:nil];
    
    
}

- (IBAction)didRemoveOneWrong:(id)sender {
    [Flurry logEvent:@"RemoveOneWrong"];
    [FIRAnalytics logEventWithName:@"RemoveOneWrong" parameters:nil];
    [YMMYandexMetrica reportEvent:@"RemoveOneWrong" parameters:nil
                        onFailure:^(NSError * _Nonnull error) {}];
    [_delegate gameHelpDidRemoveOndeWrong];
    [_delegate gameHelpClose];
}

- (IBAction)didRemoveAllCells:(id)sender {
    [Flurry logEvent:@"http://iosgames.ru/?page_id=13324"];
    [FIRAnalytics logEventWithName:@"Подсказки FV" parameters:nil];
    [YMMYandexMetrica reportEvent:@"Подсказки" parameters:nil
                        onFailure:^(NSError * _Nonnull error) {}];
    if ([[AppSettings currentLanguage] isEqualToString:@"ru_RU"])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlRUS]];
    }
    else{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlENG]];
    }
//    [_delegate gameHelpDidRemoveAllCells];
    [_delegate gameHelpClose];
}

- (IBAction)didCoins:(id)sender {
    [_delegate gameHelpDidCoins];
    [_delegate gameHelpClose];
}

#pragma mark ShareDelegate
- (void)sharerStartedSending:(SHKSharer *)sharer
{
	if (!sharer.quiet)
		[[SHKActivityIndicator currentIndicator] displayActivity:SHKLocalizedString(@"Saving to %@", [[sharer class] sharerTitle]) forSharer:sharer];
}


- (void)sharerFinishedSending:(SHKSharer *)sharer
{
    if (!sharer.quiet)
        [[SHKActivityIndicator currentIndicator] displayCompleted:SHKLocalizedString(@"Saved!") forSharer:sharer];
    
    [_delegate gameHelpClose];
}

- (void)sharer:(SHKSharer *)sharer failedWithError:(NSError *)error shouldRelogin:(BOOL)shouldRelogin
{
    
    [[SHKActivityIndicator currentIndicator] hideForSharer:sharer];
    
//    //if user sent the item already but needs to relogin we do not show alert
//    if (!sharer.quiet && sharer.pendingAction != SHKPendingShare && sharer.pendingAction != SHKPendingSend)
//	{
		[[[UIAlertView alloc] initWithTitle:SHKLocalizedString(@"Error")
                                    message:sharer.lastError!=nil?[sharer.lastError localizedDescription]:SHKLocalizedString(@"There was an error while sharing")
                                   delegate:nil
                          cancelButtonTitle:SHKLocalizedString(@"Close")
                          otherButtonTitles:nil] show];
//    }
//    if (shouldRelogin) {
//        [sharer promptAuthorization];
//	}
}

- (void)sharerCancelledSending:(SHKSharer *)sharer
{
    
}

- (void)sharerAuthDidFinish:(SHKSharer *)sharer success:(BOOL)success
{
    
}

@end
