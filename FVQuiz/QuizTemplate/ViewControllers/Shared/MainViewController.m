//
//  MainViewController.m
//  QuizTemplate
//
//  Created by Uladzislau Yasnitski on 11/12/13.
//  Copyright (c) 2013 Uladzislau Yasnitski. All rights reserved.
//

#import "MainViewController.h"
#import "Model.h"
#import "MBProgressHUD.h"
#import <Chartboost/Chartboost.h>
#import "Localization.h"
#import "UIColor+NewColor.h"
#import "UIFont+CustomFont.h"
#import "Person.h"
#import "QuestionNew.h"
#import "PlayingPerson.h"
#import "MKStoreManager.h"
#import "GameModel.h"
#import "AppSettings.h"
#import "Flurry.h"
#import "PresentCoinsManager.h"
#import "ConstantsAndMacros.h"
//#import "SAAdManager.h"
#import "VKSdk.h"
#import "VkInviteViewController.h"
//#import "DEFacebookComposeViewController.h"
#import "ShareKit.h"
//#import "SHKiOSTwitter.h"
@import FirebaseAnalytics;


@interface MainViewController () <UIActionSheetDelegate> {

}
@end

@implementation MainViewController

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [GameModel sharedInstance];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startOnlineGame:) name:@"startOnlineGame" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showPresentAlert) name:@"PresentCoinsShowGift" object:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[Model instance] initializeDataWithSucces:^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } onFailed:^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
    
    _playerBid = 25;
    
    if ([[Model instance] bIsShowBanner]) {
        [Chartboost setShouldRequestInterstitialsInFirstSession:NO];
//        [Chartboost showInterstitial:CBLocationHomeScreen];
    } else {
        [self removeBanner];
    }
    
    ///
    _lbStartGame.text = [[[Localization instance] stringWithKey:@"txt_startOffline"] uppercaseString];
    _lbStartGame.textAlignment = NSTextAlignmentCenter;
    _lbStartGame.textColor = [UIColor whiteColor];
    _lbStartGame.highlightedTextColor = [UIColor lightGrayColor];
    _lbStartGame.font = [UIFont myFontSize:15];
    
    _lbStartOnline.text = [[[Localization instance] stringWithKey:@"txt_startOnline"] uppercaseString];
    _lbStartOnline.textAlignment = NSTextAlignmentCenter;
    _lbStartOnline.textColor = [UIColor whiteColor];
    _lbStartOnline.highlightedTextColor = [UIColor lightGrayColor];
    _lbStartOnline.font = [UIFont myFontSize:12];

    if ([[AppSettings currentLanguage] isEqualToString:@"ru_RU"]){
        [self.btnMoreApp setImage:[UIImage imageNamed:@"quiz.png"] forState:UIControlStateNormal];
    }
    else{
        [self.btnMoreApp setImage:[UIImage imageNamed:@"quiz_en.png"] forState:UIControlStateNormal];
    }
    
    [PresentCoinsManager appLaunch];
}

-(void)alertDidDismissed:(AlertViewController*)alert{
    [[self currentViewController] dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

-(void)alertControllerDidClose:(AlertViewController*)alert{
    if(alert.tag == 667){
        [[self currentViewController] dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    }
    else if(alert.tag != 666){
        Person *person = [[Person allObjects] lastObject];
        
        NSInteger earnedPoints = [person.earnedPoints integerValue];
        earnedPoints += 50;
        person.earnedPoints = [NSNumber numberWithInteger:earnedPoints];
        
        [DELEGATE saveContext];
        [self updateCoinsForLabel:self.lbCoins withAnimation:YES];
        [[self currentViewController] dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    }
    else if (alert.tag == 666){
        [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
        self.view.userInteractionEnabled = NO;
        
        double delayInSeconds = 1;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            self.view.userInteractionEnabled = YES;
            [self didCoins];
        });
    }
    else{
        [[self currentViewController] dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    }
    
}


-(void)showPresentAlert
{
    NSString *msg = [NSString stringWithFormat:[[Localization instance] stringWithKey:@"txt_firstLaunch"],isPad ? @"iPad" : @"iPhone"];
    
    AlertViewController *alert = [[AlertViewController alloc] initWithMessage:msg buttonTitle:@"OK"];
    alert.tag = 999;
    alert.delegate = self;
    UINavigationController *navVc = [[UINavigationController alloc] initWithRootViewController:alert];
    [navVc setNavigationBarHidden:YES];
    
    [[self currentViewController] presentPopupViewController:navVc animationType:MJPopupViewAnimationFade];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
/*
- (IBAction)click_btn_vk:(id)sender {
    [Flurry logEvent:urlVK];
    [FIRAnalytics logEventWithName:@"urlVK" parameters:nil];
    [self showVK:sender];
}

- (IBAction)click_btn_FB:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:[[Localization instance] stringWithKey:@"txt_shareToFacebook"] delegate:self cancelButtonTitle:[[Localization instance] stringWithKey:@"txt_cancel"] destructiveButtonTitle:nil otherButtonTitles:[[Localization instance] stringWithKey:@"txt_buyCoins"], nil];
    [actionSheet showInView:self.navigationController.view];
}

- (IBAction)click_btn_TW:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:[[Localization instance] stringWithKey:@"txt_shareToTw"] delegate:self cancelButtonTitle:[[Localization instance] stringWithKey:@"txt_cancel"] destructiveButtonTitle:nil otherButtonTitles:[[Localization instance] stringWithKey:@"txt_buyCoins"], nil];
    actionSheet.tag = 1;
    [actionSheet showInView:self.navigationController.view];
}
*/

- (IBAction)click_btn_vk:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlVK]];
}

- (IBAction)click_btn_FB:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlFB]];
}

- (IBAction)click_btn_TW:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlTW]];
}

- (IBAction)didLink:(id)sender {
    [Flurry logEvent:@"http://iosgames.ru/"];
    [FIRAnalytics logEventWithName:@"iosgames" parameters:nil];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://iosgames.ru/"]];
}
- (IBAction)didStartGame:(id)sender {
    if ([[[[Person allObjects] lastObject] allPersonPoints] integerValue] <= 0) {
        [self showNoCoinsAlert];
    } else {
        [self startOfflineGame];
    }
}

-(void)startOfflineGame
{
    // template
}

- (IBAction)didStartOnlineGame:(id)sender {
    
    // template
}

-(void)stavkaViewDidClose {
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

-(void)stavkaViewDidBid:(NSInteger)bid {
    _playerBid = bid;
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideLeftRight];
    [[GameModel sharedInstance] startOnlineGameWithViewController:self];
}

- (IBAction)didLeaderBoard:(id)sender {
    GKLeaderboardViewController *leaderboardController = [[GKLeaderboardViewController alloc] init] ;
	if (leaderboardController != NULL)
	{
		leaderboardController.category = kLeaderboardID;
		leaderboardController.timeScope = GKLeaderboardTimeScopeWeek;
		leaderboardController.leaderboardDelegate = self;
		[self presentViewController:leaderboardController animated:YES completion:nil];
	}
    
}

-(void)leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)removeBanner
{
    [self.admobBannerView removeFromSuperview];
    self.admobBannerView.delegate = nil;
    self.admobBannerView = nil;
    
    [_btnRemoveAds removeFromSuperview];
}
- (IBAction)didRemoveAds:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[MKStoreManager sharedManager] buyFeature:APP_REMOVE_ADS onComplete:^(NSString *purchasedFeature, NSData *purchasedReceipt, NSArray *availableDownloads) {
        
        [self removeBanner];
        
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } onCancelled:^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
    
}
- (IBAction)click_btn_quez:(id)sender {
    if ([[AppSettings currentLanguage] isEqualToString:@"ru_RU"]){
        [FIRAnalytics logEventWithName:@"Our_Apps" parameters:nil];
        [Flurry logEvent:@"Наши игры"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlOtherQu]];
    }
    else{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlOtherQuEN]];
    }
}

- (IBAction)didInfo:(id)sender {
    AlertViewController *alert = [[AlertViewController alloc] initWithMessage:[[Localization instance] stringWithKey:@"txt_alertInfo"] buttonTitle:@"OK"];
    alert.delegate = self;
    alert.tag = 667;
    alert.showMailController = YES;
    UINavigationController *navVc = [[UINavigationController alloc] initWithRootViewController:alert];
    [navVc setNavigationBarHidden:YES];
    
    [[self currentViewController] presentPopupViewController:navVc animationType:MJPopupViewAnimationFade];
}

- (void)startOnlineGame:(NSNotification*)notif {
    NSDictionary *result = notif.userInfo;
    NSString *ids = [result objectForKey:@"questionIds"];
    BOOL server = [[result objectForKey:@"IsServer"] boolValue];
    
    [self startGameWithQuestionIds:ids isServer:server];
}

- (void)startGameWithQuestionIds:(NSString*)ids isServer:(BOOL)isServer {
    // template
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    if (actionSheet.tag) {
//        [self shareToTw];
        return;
    }
//    [self shareToFB];
}

#pragma mark - Social Networks
/*
- (void)shareToFB {
    DEFacebookComposeViewController *facebookViewComposer = [[DEFacebookComposeViewController alloc] init] ;
    [facebookViewComposer addImage:[UIImage imageNamed:shareImageName]];
    [facebookViewComposer addURL:[NSURL URLWithString:iTunesLink]];
    [facebookViewComposer setInitialText:[NSString stringWithFormat:@"%@",[[Localization instance] stringWithKey:@"txt_postTitle"]]];

    [facebookViewComposer setCompletionHandler:^(DEFacebookComposeViewControllerResult result) {
        switch (result) {
            case DEFacebookComposeViewControllerResultCancelled:
                break;
            case DEFacebookComposeViewControllerResultDone: {
                [[GameModel sharedInstance] earnCoins:fbShareErnedCoins];
                [self updateCoinsForLabel:self.lbCoins withAnimation:YES];
                    [Flurry logEvent:@"Did share FB"];
                    [FIRAnalytics logEventWithName:@"Share_FB" parameters:nil];
            }
                break;
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];

        [self.navigationController dismissViewControllerAnimated:YES completion:^{

        }];

    }];

    [self.navigationController presentViewController:facebookViewComposer animated:YES completion:nil];
}

- (void)shareToTw {
    UIImage *myImage = [UIImage imageNamed:shareImageName];
    
    NSString *myTitle = [NSString stringWithFormat:@"%@",[[Localization instance] stringWithKey:@"txt_postTitle"]];
    
    SHKItem *item = [SHKItem image:myImage title:myTitle];
    item.URL = [NSURL URLWithString:iTunesLink];
    SHKiOSTwitter *sharer = [SHKiOSTwitter shareItem:item];
    sharer.shareDelegate = self;
}

- (void)sharerFinishedSending:(SHKSharer *)sharer {

    [[GameModel sharedInstance] earnCoins:twShareErnedCoins];
    [Flurry logEvent:@"Did share Tw"];
    [FIRAnalytics logEventWithName:@"Share_Tw" parameters:nil];
    [self updateCoinsForLabel:self.lbCoins withAnimation:YES];
}
*/
#pragma mark add

-(UIViewController*) findBestViewController:(UIViewController*)vc {
    
    if (vc.presentedViewController) {
        
        // Return presented view controller
        return [self findBestViewController:vc.presentedViewController];
        
    } else if ([vc isKindOfClass:[UISplitViewController class]]) {
        
        // Return right hand side
        UISplitViewController* svc = (UISplitViewController*) vc;
        if (svc.viewControllers.count > 0)
            return [self findBestViewController:svc.viewControllers.lastObject];
        else
            return vc;
        
    } else if ([vc isKindOfClass:[UINavigationController class]]) {
        
        // Return top view
        UINavigationController* svc = (UINavigationController*) vc;
        if (svc.viewControllers.count > 0)
            return [self findBestViewController:svc.topViewController];
        else
            return vc;
        
    } else if ([vc isKindOfClass:[UITabBarController class]]) {
        
        // Return visible view
        UITabBarController* svc = (UITabBarController*) vc;
        if (svc.viewControllers.count > 0)
            return [self findBestViewController:svc.selectedViewController];
        else
            return vc;
        
    } else {
        
        // Unknown view controller type, return last child view controller
        return vc;
        
    }
    
}

-(UIViewController*) currentViewController {
    
    // Find best view controller
    UIViewController* viewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    return [self findBestViewController:viewController];
    
}

@end
