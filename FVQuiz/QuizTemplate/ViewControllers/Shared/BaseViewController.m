//
//  BaseViewController.m
//  QuizTemplate
//
//  Created by Uladzislau Yasnitski on 11/12/13.
//  Copyright (c) 2013 Uladzislau Yasnitski. All rights reserved.
//

#import "BaseViewController.h"
#import "UIViewController+UpdateCoins.h"
#import "Person.h"
#import "AppSettings.h"
#import "Model.h"
#import "MainViewController.h"
#import "UIColor+NewColor.h"
#import <Tapjoy/Tapjoy.h>
#import <Chartboost/Chartboost.h>
#import "Localization.h"
#import <AdWowSDK/AdWowSDK.h>
#import "AdToAppSDK.h"
#import "ConstantsAndMacros.h"
#import "VKSdk.h"
#import "Flurry.h"
#import "VkInviteViewController.h"
#import "GameModel.h"
#import <UnityAds/UnityAds.h>
#import <YandexMobileAds/YandexMobileAds.h>
#import <YandexMobileMetrica/YMMYandexMetrica.h>
#import <YandexMobileMetrica/YMMYandexMetricaConfiguration.h>
@import FirebaseAnalytics;

@interface BaseViewController ()<GADInterstitialDelegate, GADRewardBasedVideoAdDelegate, AWNotificationDelegate, AWFormDelegate, AWUnitDelegate, AdToAppSDKDelegate, StavkaViewControllerDelegate, UnityAdsDelegate, YMAInterstitialDelegate, TJPlacementDelegate> {
    GADInterstitial *_interstitial;
}

@property (nonatomic, strong) YMAInterstitialController *interstitialAd;

@end

@implementation BaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(timerStep:) name:@"CoinTimerStep" object:nil];
        ///[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addOneCoin) name:@"AddOneCoinNotification" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUpdatedPoints:) name:TJC_GET_CURRENCY_RESPONSE_NOTIFICATION object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(vkDidAuthorized) name:@"vkDidAuthorized" object:nil];

    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)getUpdatedPoints:(NSNotification*)notifyObj
{
	NSNumber *tapPoints = notifyObj.object;
    Person *person = [[Person allObjects] lastObject];
    
    person.tapjoyedPoints = tapPoints;
    
    [DELEGATE saveContext];
    
    [self updateCoinsForLabel:_lbCoins withAnimation:YES];
    
}

-(void)removeBanner
{
    [self.admobBannerView removeFromSuperview];
    self.admobBannerView.delegate = nil;
    self.admobBannerView = nil;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
   
    [Tapjoy getCurrencyBalance];
   
    [_btntopCoins setTitle:nil forState:UIControlStateNormal];
    [GADRewardBasedVideoAd sharedInstance].delegate = self;
    if (![[GADRewardBasedVideoAd sharedInstance] isReady]) {
        [self requestRewardedVideo];
    }

//    self.view.backgroundColor = [UIColor colorWith8BitRed:229 green:229 blue:229 alpha:1];
//  self.view.backgroundColor = [UIColor colorWith8BitRed:161 green:230 blue:229 alpha:1];
//  self.view.backgroundColor = [UIColor colorWith8BitRed:190 green:226 blue:222 alpha:1];
    
    _lbCoins.text = nil;
    _lbCoins.textColor = [UIColor whiteColor];
    _lbCoins.textAlignment = NSTextAlignmentCenter;
    _lbCoins.adjustsFontSizeToFitWidth = YES;
    [_lbCoins setFont:[UIFont myFontSize:isPad ? 24 : 12]];
    
    _lbPoints.text = nil;
    _lbPoints.textColor = [UIColor whiteColor];
    _lbPoints.textAlignment = NSTextAlignmentCenter;
    _lbPoints.adjustsFontSizeToFitWidth = YES;
    [_lbPoints setFont:[UIFont myFontSize:isPad ? 24 : 12]];
    _coinsProgress = [[UIProgressView alloc] initWithFrame:CGRectMake(_lbCoins.frame.origin.x, _lbCoins.frame.origin.y + _lbCoins.frame.size.height, _lbCoins.frame.size.width, 2)];
    [_coinsProgress setProgressTintColor:[UIColor myGreenColor]];
    //[self.view addSubview:_coinsProgress];
    
    
    if ([[Model instance] bIsShowBanner])
    {
        [self createADBannerView];
    }
    else
    {
        [self removeBanner];
    }
}

- (void)createADBannerView {
    
    if (self.admobBannerView)
        [self.admobBannerView removeFromSuperview];
    
//    GADAdSize adSize = kGADAdSizeBanner;//kGADAdSizeSmartBannerPortrait;
    
    self.admobBannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait] ;
    
   
//    CGRect bannerFrame = self.admobBannerView.frame;
//
//    CGFloat bottomPadding=0;
//    if (@available(iOS 11.0, *)) {
//        UIWindow *window = UIApplication.sharedApplication.keyWindow;
//        CGFloat topPadding = window.safeAreaInsets.top;
//        bottomPadding = window.safeAreaInsets.bottom;
//    }
//
//    bannerFrame.origin = CGPointMake(0, self.view.frame.size.height-bottomPadding);
////    bannerFrame.origin = CGPointMake(0, 0);
//    self.admobBannerView.frame = bannerFrame;
     [self addBannerViewToView:self.admobBannerView];
    // 3
    
    self.admobBannerView.adUnitID = AdMob_ID;
    

    self.admobBannerView.rootViewController = self;
    self.admobBannerView.delegate = self;
    
    // 4
    
//    [self.view addSubview:self.admobBannerView];
    [self.admobBannerView loadRequest:[GADRequest request]];
    
    
//    [self updateFrameWithAdmobBanner];
    
}

-(void)addBannerViewToView:(UIView *_Nonnull)bannerView {
    bannerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview: bannerView];
    if (@available(ios 11.0, *)) {
        [self positionBannerViewAtBottomOfSafeArea:bannerView];
    } else {
        [self positionBannerViewAtBottomOfView:bannerView];
    }
}

- (void)positionBannerViewAtBottomOfSafeArea:(UIView *_Nonnull)bannerView NS_AVAILABLE_IOS(11.0) {
    // Position the banner. Stick it to the bottom of the Safe Area.
    // Centered horizontally.
    UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
    [NSLayoutConstraint activateConstraints:@[
                                              [bannerView.centerXAnchor constraintEqualToAnchor:guide.centerXAnchor],
                                              [bannerView.bottomAnchor constraintEqualToAnchor:guide.bottomAnchor]
                                              ]];
}

- (void)positionBannerViewAtBottomOfView:(UIView *_Nonnull)bannerView {
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.bottomLayoutGuide
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1
                                                           constant:0]];
}

- (void)showInterstitial {
    NSDate *today = [NSDate date];
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorian components:NSDayCalendarUnit fromDate:today];
    BOOL even = components.day % 2;
    if (!even) {
        [self showAdMob];
    } else {
        [self showAdMob];
    }
}

- (void)showYandex {
    self.interstitialAd = [[YMAInterstitialController alloc] initWithBlockID:YandexIntBannerID];
    self.interstitialAd.delegate = self;
    [self.interstitialAd load];
}

- (void)interstitialDidLoadAd:(YMAInterstitialController *)interstitial
{
    [interstitial presentInterstitialFromViewController:self];
}

- (void)interstitialDidFailToLoadAd:(YMAInterstitialController *)interstitial error:(NSError *)error
{
    NSLog(@"Loading failed. Error: %@", error);
    [FIRAnalytics logEventWithName:@"NoShowYandex" parameters:nil];
    [YMMYandexMetrica reportEvent:@"Yandex Loading failed" parameters:nil
                        onFailure:^(NSError * _Nonnull error) {}];
 //   [self showAdToApp];
}

- (void)interstitialWillLeaveApplication:(YMAInterstitialController *)interstitial
{
    NSLog(@"Will leave application");
}

- (void)interstitialDidFailToPresentAd:(YMAInterstitialController *)interstitial error:(NSError *)error
{
    NSLog(@"Failed to present interstitial. Error: %@", error);
    [FIRAnalytics logEventWithName:@"Failed to present interstitial" parameters:nil];
    [YMMYandexMetrica reportEvent:@"Failed to present interstitial" parameters:nil
                        onFailure:^(NSError * _Nonnull error) {}];
    [self showAdMob];
    [YMMYandexMetrica reportEvent:@"showAdMob_Int" parameters:nil
                        onFailure:^(NSError * _Nonnull error) {}];
}

- (void)interstitialWillAppear:(YMAInterstitialController *)interstitial
{
    NSLog(@"Interstitial will appear");
    [FIRAnalytics logEventWithName:@"YandexWillAppear" parameters:nil];
    [YMMYandexMetrica reportEvent:@"YandexWillAppear" parameters:nil
                        onFailure:^(NSError * _Nonnull error) {}];
}

- (void)interstitialDidAppear:(YMAInterstitialController *)interstitial
{
    NSLog(@"Interstitial did appear");
    [FIRAnalytics logEventWithName:@"YandexDidAppear" parameters:nil];
    [YMMYandexMetrica reportEvent:@"Failed to present interstitial" parameters:nil
                        onFailure:^(NSError * _Nonnull error) {}];
}

- (void)interstitialWillDisappear:(YMAInterstitialController *)interstitial
{
    NSLog(@"Interstitial will disappear");
}

- (void)interstitialDidDisappear:(YMAInterstitialController *)interstitial
{
    NSLog(@"Interstitial did disappear");
}

- (void)interstitialWillPresentScreen:(UIViewController *)webBrowser
{
    NSLog(@"Interstitial will present screen");
}

- (void)showAdToApp {
    [AdToAppSDK setDelegate:self];
    [AdToAppSDK showInterstitial:ADTOAPP_IMAGE_INTERSTITIAL];
}

//- (void)showChartboost {
//    [Chartboost showInterstitial:CBLocationDefault];
//}

- (void)showAdMob {
        _interstitial = [[GADInterstitial alloc] init];
        _interstitial.adUnitID = AdMobIntersential_ID;
        _interstitial.delegate = self;
        
        [_interstitial loadRequest:[GADRequest request]];
}

- (void)showTapjoy {
    TJPlacement *placement = [TJPlacement placementWithName:TapJoyPlace1 delegate:self ];
    if (placement.isContentReady) {
        [placement showContentWithViewController: self.navigationController];
    } else {
        [placement requestContent];
    }
}

#pragma mark - TJPlacementDelegate
- (void)requestDidSucceed:(TJPlacement*)placement{
    NSLog(@"Tapjoy request success");
}

- (void)requestDidFail:(TJPlacement*)placement error:(NSError*)error{
    NSLog(@"Tapjoy error %@", error);
}

- (void)contentIsReady:(TJPlacement*)placement{
    [placement showContentWithViewController: self.navigationController];
} //This is called when the content is actually available to display.

//#pragma mark AdToAppSDKDelegate
//-(void)onAdDidDisappear:(NSString*)adType {

//}

//-(void)onReward:(int)reward currency:(NSString*)gameCurrency {

//}

//-(void)onAdWillAppear:(NSString*)adType {
//    NSLog(@"On AdToApp Interstitial Show");
//}

#pragma mark - GADInterstitialDelegate
- (void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    [_interstitial presentFromRootViewController:self.navigationController];
}

- (void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"adMob error %@", error);
}

-(void)updateFrameWithAdmobBanner
{
    // template
}

#pragma mark - AdMobBannerViewDelegate
-(void)adViewDidReceiveAd:(GADBannerView *)view
{
//    [self.admobBannerView removeFromSuperview];
//
//    [UIView beginAnimations:@"animatedBannerOn" context:nil];
//
//    CGRect bannerFrame = self.admobBannerView.frame;
//
//
//    CGFloat bottomPadding=0;
//    if (@available(iOS 11.0, *)) {
//        UIWindow *window = UIApplication.sharedApplication.keyWindow;
//        CGFloat topPadding = window.safeAreaInsets.top;
//        bottomPadding = window.safeAreaInsets.bottom;
//    }
//    bannerFrame.origin = CGPointMake((self.view.frame.size.width - bannerFrame.size.width)/2, self.view.frame.size.height - bannerFrame.size.height-bottomPadding);
//
//    self.admobBannerView.frame = bannerFrame;
//
//    //    bannerFrame.origin = CGPointMake(0, self.view.frame.size.height-bottomPadding);
//
//    [self.view addSubview:self.admobBannerView];
//    [self.view bringSubviewToFront:self.admobBannerView];
//    [UIView commitAnimations];
    
}


- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error {
    
    self.admobBannerView.delegate = nil;
    self.admobBannerView = nil;
    
}

#pragma mark - AdWow
- (void)showAdWowFullSize {
    void (^handler)(AWUnit *, NSError *) = ^(AWUnit *unit, NSError *error) {
        if (error) {
            NSLog(@"AdWow error %@", error);
            return;
        }
        if (unit) {
            unit.delegate = self;
            unit.form.delegate = self;
            [unit show];
        }
    };
    [[AdWow sharedInstance] saveMoment:[[Localization instance] stringWithKey:@"txt_AdWowMomentName"] withCompletionHandler: handler];
}

- (void)showAdWowNotification {
    void (^handler)(AWUnit *, NSError *) = ^(AWUnit *unit, NSError *error) {
        if (error) {
            NSLog(@"AdWow error %@", error);
            return;
        }
        if (unit) {
            AWNotification * n = [AWNotification new];
            [AdWow sharedInstance].notificationView = nil;
            n.title = [[Localization instance] stringWithKey:@"txt_AdWowMomentName"];
//            n.icon = [UIImage imageNamed:@"Icon.png"];
            n.message = [[Localization instance] stringWithKey:@"txt_AdWowMomentMessage"];
            n.sticky = AWS_Top;
            n.defaultBackgroundColor = [UIColor lightGrayColor];
            n.delegate = self;
            unit.notification = n;
            unit.delegate = self;
            unit.form.delegate = self;
            [unit show];
        }
    };
        [[AdWow sharedInstance] saveMoment:[[Localization instance] stringWithKey:@"txt_AdWowMomentName"] withCompletionHandler: handler];
}

#pragma mark -
#pragma mark AWUnitDelegate

- (void) willPresentUnit:(AWUnit *)unit {
    NSLog(@"willPresentUnit %@", unit);
}

- (void) didDismissUnit:(AWUnit *)unit {
    NSLog(@"didDismissUnit %@", unit);
}


#pragma mark -
#pragma mark AWNotificationDelegate

- (void) willPresentNotification:(AWNotification *)notification {
    NSLog(@"willPresentNotification %@", notification);
}

- (void) didDismissNotification:(AWNotification *)notification {
    NSLog(@"didDismissNotification %@", notification);
}

- (void) didDismissNotificationWithClick:(AWNotification *)notification {
    NSLog(@"didDismissNotificationWithClick %@", notification);
}

#pragma mark -
#pragma mark AWFormDelegate

- (void) willPresentForm:(AWForm *)form {
    NSLog(@"willPresentForm %@", form);
}

- (void) didDismissForm:(AWForm *)form {
    NSLog(@"didDismissForm %@", form);
}

#pragma mark - Actions
- (IBAction)didCoins {
    GetCoinsViewController *vc = [[GetCoinsViewController alloc] initWithNibName:isPad ? @"GetCoinsViewController_iPad" : @"GetCoinsViewController_iPhone" bundle:nil];
    vc.delegate = self;
    UINavigationController *navVc = [[UINavigationController alloc] initWithRootViewController:vc];
    [navVc setNavigationBarHidden:YES];
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [delegate.window.rootViewController presentPopupViewController:navVc animationType:MJPopupViewAnimationSlideRightRight dismissed:^{
        [self updateCoinsForLabel:_lbCoins withAnimation:YES];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)timerStep:(NSNotification*)notification
{
//    NSDictionary *dic = notification.userInfo;
//    if (![dic count])
//        return;
//    NSInteger timerValue = [[dic objectForKey:@"timerValue"] integerValue];
//    
//    [_coinsProgress setProgress:(float)timerValue / 60];
}
-(void)addOneCoin
{
    [self updateCoinsForLabel:_lbCoins withAnimation:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateCoinsForLabel:_lbCoins withAnimation:NO];
    [self updateGameCenterPointsForLabel:_lbPoints withAnimation:YES];
 
       
    
    
    
}

-(void)showNoCoinsAlert
{
    AlertViewController *alert = [[AlertViewController alloc] initWithMessage:[[Localization instance] stringWithKey:@"txt_noCoins"] buttonTitle:[[Localization instance] stringWithKey:@"txt_buyCoins"]];
    alert.tag = 666;
    alert.delegate = self;
    UINavigationController *navVc = [[UINavigationController alloc] initWithRootViewController:alert];
    [navVc setNavigationBarHidden:YES];

    [self presentPopupViewController:navVc animationType:MJPopupViewAnimationFade];
}

- (void)alertControllerDidClose:(AlertViewController *)alert {
  
    if (alert.tag == 666) {
        [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
        [self performSelector:@selector(didCoins) withObject:nil afterDelay:1];
        
    } else if (alert.tag == 999) {
        Person *person = [[Person allObjects] lastObject];

        NSInteger earnedPoints = [person.earnedPoints integerValue];
        earnedPoints += 50;
        person.earnedPoints = [NSNumber numberWithInteger:earnedPoints];

        [DELEGATE saveContext];
        [self updateCoinsForLabel:self.lbCoins withAnimation:YES];
        [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    } else
        [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

-(void)alertDidDismissed:(AlertViewController *)alert
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

-(void)getCoinsDismissed
{
    [self updateCoinsForLabel:_lbCoins withAnimation:YES];
    
    if (![[Model instance] bIsShowBanner])
    {
        [self removeBanner];
    }
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideRightRight];
}

- (void)getCoinsDidVideo {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:[[Localization instance] stringWithKey:@"txt_presentRVAction"] delegate:self cancelButtonTitle:[[Localization instance] stringWithKey:@"txt_cancel"] otherButtonTitles:@"OK", nil];
    [alertView show];
}

- (void)loadVideo {
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideRightRight];
    NSDate *today = [NSDate date];
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorian components:NSDayCalendarUnit fromDate:today];
    BOOL even = components.day % 2;
    if (!even) {
        [self showVideoRW];
    } else {
        [self showVideoRW];
    }
}

#pragma mark - Local Notifications

- (void)stavkaViewDidClose {
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

- (void)stavkaViewDidBid:(NSInteger)bid {
}

- (IBAction)showVK:(id)sender {
    if (![[AppSettings currentLanguage] isEqualToString:@"ru_RU"]){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlVK]];
        return;
    }

    NSString *nibName = isPad ? @"StavkaViewController_iPad" : @"StavkaViewController_iPhone";
    NSString *authTitle = [VKSdk isLoggedIn] ? @"Авторизация                Выйти" : @"Авторизация                Войти";

    StavkaViewController *vc = [[StavkaViewController alloc] initWithNibName:nibName
                                                                      bundle:nil
                                                                     buttons:@[authTitle,
                                                                               @"Вступить в группу    +20 монет",
                                                                               @"Поделиться             +30 монет",
                                                                               @"Пригласить друга    +40 монет"]];
    vc.delegate = self;
    UINavigationController *navVc = [[UINavigationController alloc] initWithRootViewController:vc];
    [navVc setNavigationBarHidden:YES];

    [self presentPopupViewController:navVc animationType:MJPopupViewAnimationSlideLeftRight];
}

//- (void)showVideo {
//    [self showUnity];
//    [self showChartboost];
//}

- (void)requestRewardedVideo {
    GADRequest *request = [GADRequest request];
    [[GADRewardBasedVideoAd sharedInstance] loadRequest:request
                                           withAdUnitID:AdMob_RW];
    [GADRewardBasedVideoAd sharedInstance].delegate = self;
}



- (void)showVideoRW {
    if ([[GADRewardBasedVideoAd sharedInstance] isReady]) {
        [[GADRewardBasedVideoAd sharedInstance] presentFromRootViewController:self];
        [FIRAnalytics logEventWithName:@"ShowAdMobVideoFV" parameters:nil];
        [YMMYandexMetrica reportEvent:@"ShowAdMobVideo" parameters:nil
                            onFailure:^(NSError * _Nonnull error) {}];
    } else {
        [self showUnity];
        [FIRAnalytics logEventWithName:@"NoShowAdMobVideo" parameters:nil];
        [YMMYandexMetrica reportEvent:@"NoShowAdMobVideo" parameters:nil
                            onFailure:^(NSError * _Nonnull error) {}];
    }
}

#pragma mark GADRewardBasedVideoAdDelegate implementation
- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
   didRewardUserWithReward:(GADAdReward *)reward {
    NSString *rewardMessage =
    [NSString stringWithFormat:@"Reward received with currency %@ , amount %lf", reward.type,
     [reward.amount doubleValue]];
    NSLog(@"%@", rewardMessage);
    // Reward the user for watching the video.
    [self didRewardPoints:unityReward];
}

- (void)rewardBasedVideoAdDidReceiveAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad is received.");
}

- (void)rewardBasedVideoAdDidOpen:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Opened reward based video ad.");
}

- (void)rewardBasedVideoAdDidStartPlaying:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad started playing.");
}

- (void)rewardBasedVideoAdDidClose:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad is closed.");
}

- (void)rewardBasedVideoAdWillLeaveApplication:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad will leave application.");
}

- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
    didFailToLoadWithError:(NSError *)error {
    NSLog(@"Reward based video ad failed to load.");
}


- (void)showChartboost {
    [Chartboost startWithAppId:ChartboostAppId appSignature:ChartboostSignature delegate:self];
    [Chartboost cacheRewardedVideo:CBLocationMainMenu];
    if([Chartboost hasRewardedVideo:CBLocationMainMenu]) {        
        [Chartboost showRewardedVideo:CBLocationMainMenu];
        [FIRAnalytics logEventWithName:@"showChartboost" parameters:nil];
        [YMMYandexMetrica reportEvent:@"showChartboost" parameters:nil
                            onFailure:^(NSError * _Nonnull error) {}];
    }
    else {
        // Прямо сейчас у нас нет кэшированного видео, но мы пытаемся получить его для следующего раза
        [FIRAnalytics logEventWithName:@"NoShowChartboost" parameters:nil];
        [YMMYandexMetrica reportEvent:@"NoShowChartboost" parameters:nil
                            onFailure:^(NSError * _Nonnull error) {}];
        [Chartboost cacheRewardedVideo:CBLocationMainMenu];
        [self showUnity];
    }
}

#pragma mark ChartboostDelegate
//- (void)didFailToLoadRewardedVideo:(CBLocation)location
//                         withError:(CBLoadError)error {
//    [self showUnity];
//}

- (void)didCompleteRewardedVideo:(CBLocation)location withReward:(int)reward {
    [self didRewardPoints:reward * chartboostReward];
}

- (void)didCloseRewardedVideo:(CBLocation)location {
    [self didRewardPoints:0];
}

- (void)showUnity {
    [[UnityAds sharedInstance] setViewController:self.navigationController];
    [[UnityAds sharedInstance] setDelegate:self];
    [[UnityAds sharedInstance] setZone:@"rewardedVideo"];
    [FIRAnalytics logEventWithName:@"showUnity" parameters:nil];
    [YMMYandexMetrica reportEvent:@"showUnity" parameters:nil
                        onFailure:^(NSError * _Nonnull error) {}];
    if ([[UnityAds sharedInstance] canShow]) {
        // If both are ready, show the ad.
        [[UnityAds sharedInstance] show];
    } else {
        NSLog(@"UnityAds cant be shown");
        [FIRAnalytics logEventWithName:@"NoShowUnity" parameters:nil];
        [YMMYandexMetrica reportEvent:@"NoShowUnity" parameters:nil
                            onFailure:^(NSError * _Nonnull error) {}];
        [self showTapjoyRW];
    }
}

- (void)showAdToAppVideo {
    [AdToAppSDK setDelegate:self];
    [AdToAppSDK showInterstitial:ADTOAPP_REWARDED_INTERSTITIAL];//ADTOAPP_VIDEO_INTERSTITIAL];
    [self performSelector:@selector(checkATAIntersistal) withObject:nil afterDelay:3];
    [FIRAnalytics logEventWithName:@"showAdToApp" parameters:nil];
    [YMMYandexMetrica reportEvent:@"showAdToApp" parameters:nil
                        onFailure:^(NSError * _Nonnull error) {}];
}

- (void)checkATAIntersistal {
    if (![AdToAppSDK isInterstitialDisplayed]) {
        [self showUnity];
    }
}

#pragma mark AdToAppSDKDelegate
- (void)onAdDidDisappear:(NSString*)adType {
    [self didRewardPoints:0];
}

- (void)onReward:(int)reward currency:(NSString*)gameCurrency {
    [self didRewardPoints:reward];
}

- (void)onAdWillAppear:(NSString*)adType {
    NSLog(@"On AdToApp Interstitial Show");
}

#pragma mark UnityAdsDelegate
- (void)unityAdsVideoCompleted:(NSString *)rewardItemKey skipped:(BOOL)skipped {
    if (!skipped) {
        [self didRewardPoints:unityReward];
    }
}

- (void)didRewardPoints:(NSInteger)points {
    if (points) {
        Person *person = [[Person allObjects] lastObject];
        NSInteger earnedPoints = [person.earnedPoints integerValue];
        earnedPoints += points;
        person.earnedPoints = @(earnedPoints);

        [DELEGATE saveContext];
    }
    [self.navigationController dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

- (void)showTapjoyRW {
    TJPlacement *placement = [TJPlacement placementWithName:TapJoyPlace delegate:self ];
    if (placement.isContentReady) {
        [placement showContentWithViewController: self.navigationController];
        [FIRAnalytics logEventWithName:@"showTapjoyPause" parameters:nil];
        [YMMYandexMetrica reportEvent:@"showTapjoyPause" parameters:nil
                            onFailure:^(NSError * _Nonnull error) {}];
    } else {
        [placement requestContent];
        [FIRAnalytics logEventWithName:@"NoShowTapjoyPause" parameters:nil];
        [YMMYandexMetrica reportEvent:@"NoShowTapjoyPause" parameters:nil
                            onFailure:^(NSError * _Nonnull error) {}];
    }
}

#pragma mark Tapjoy Display Ads Delegate Methods

- (void)didReceiveAd:(TJCAdView*)adView
{
    NSLog(@"Tapjoy Display Ad Received");
}


- (void)didFailWithMessage:(NSString*)msg
{
    NSLog(@"No Tapjoy Display Ads available");
}

- (BOOL)shouldRefreshAd
{
    return YES;
}

#pragma mark Tapjoy View Delegate Methods

- (void)viewWillAppearWithType:(int)viewType
{
    NSLog(@"Tapjoy viewWillAppearWithType: %d", viewType);
}


- (void)viewDidAppearWithType:(int)viewType
{
    NSLog(@"Tapjoy viewDidAppearWithType: %d", viewType);
}


- (void)viewWillDisappearWithType:(int)viewType
{
    NSLog(@"Tapjoy viewWillDisappearWithType: %d", viewType);
}


- (void)viewDidDisappearWithType:(int)viewType
{
    NSLog(@"Tapjoy viewDidDisappearWithType: %d", viewType);
}


#pragma mark - Social Networks

- (void)vkOptionChoosen:(NSInteger)option {
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideLeftRight];
    switch (option) {
        case 0: {
            AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
            if (![delegate authorizeVK]) {
                [VKSdk forceLogout];
            }
        }
            break;
        case 1:
            [self joinGroup];
            break;
        case 2: {
            AppDelegate *delegate = [UIApplication sharedApplication].delegate;
            [delegate shareToVK];
        }
            break;
        case 3: {
            VkInviteViewController *controller = [[VkInviteViewController alloc] init];
            [self.navigationController pushViewController:controller animated:YES];
        }

        default:
            break;
    }
}

- (void)joinGroup {
    //if (self.currentGroup) {
    if (![VKSdk isLoggedIn]) {
        AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [delegate authorizeVK];
        return;
    }
    VKRequest *request = [VKRequest requestWithMethod:@"groups.isMember"
                                        andParameters:@{VK_API_GROUP_ID : @(70729007), VK_API_USER_ID : [VKSdk accessToken].userId}
                                        andHttpMethod:@"GET"];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [request executeWithResultBlock:^(VKResponse * response) {
        if ([response.json integerValue]) {
            [[[UIAlertView alloc] initWithTitle:@"Вы уже подписаны!"
                                        message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil] show];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            return;
        }
        VKRequest *request = [VKRequest requestWithMethod:@"groups.join"
                                            andParameters:@{VK_API_GROUP_ID : @(70729007)}
                                            andHttpMethod:@"GET"];
        [request executeWithResultBlock:^(VKResponse * response) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [Flurry logEvent:@"VK - Подписался на группу"];
            [FIRAnalytics logEventWithName:@"VK_iosgamesru" parameters:nil];
            [YMMYandexMetrica reportEvent:@"VK_iosgamesru" parameters:nil
                                onFailure:^(NSError * _Nonnull error) {}];
            [[GameModel sharedInstance] earnCoins:vkSibscribeErnedCoins];
            [[[UIAlertView alloc] initWithTitle:@"Вы подписаны!"
                                        message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil] show];
        } errorBlock:^(NSError * error) {
            if (error.code != VK_API_ERROR) {
                [error.vkError.request repeat];
            } else {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [[[UIAlertView alloc] initWithTitle:@"Ошибка"
                                            message:[NSString stringWithFormat:@"Не удалось подписаться\n%@", [error localizedDescription]]
                                           delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil] show];
                NSLog(@"VK error: %@", error);
            }
        }];   //}
    } errorBlock:^(NSError * error) {
        if (error.code != VK_API_ERROR) {
            [error.vkError.request repeat];
        } else {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [[[UIAlertView alloc] initWithTitle:@"Ошибка"
                                        message:[NSString stringWithFormat:@"Не удалось проверить подписку\n%@", [error localizedDescription]]
                                       delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil] show];
            NSLog(@"VK error: %@", error);
        }
    }];   //}
}

- (void)vkDidAuthorized {
    if (![VKSdk isLoggedIn]) {
        return;
    }
    [Flurry logEvent:@"VK - Авторизация"];
    [FIRAnalytics logEventWithName:@"VK_Auth" parameters:nil];
    [YMMYandexMetrica reportEvent:@"VK_Auth" parameters:nil
                        onFailure:^(NSError * _Nonnull error) {}];
    [[GameModel sharedInstance] earnCoins:vkAuthErnedCoins];
    [self updateCoinsForLabel:self.lbCoins withAnimation:YES];
}

- (void)showPresentAlert {
    if (self.mj_popupViewController) {
        return;
    }
    NSString *msg = [NSString stringWithFormat:[[Localization instance] stringWithKey:@"txt_firstLaunch"],isPad ? @"iPad" : @"iPhone"];

    AlertViewController *alert = [[AlertViewController alloc] initWithMessage:msg buttonTitle:@"OK"];
    alert.tag = 999;
    alert.delegate = self;
    UINavigationController *navVc = [[UINavigationController alloc] initWithRootViewController:alert];
    [navVc setNavigationBarHidden:YES];

    [self.navigationController presentPopupViewController:navVc animationType:MJPopupViewAnimationFade];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == alertView.cancelButtonIndex) {
        return;
    }
    
    [self loadVideo];
}

#pragma mark add

-(UIViewController*) findBestViewController:(UIViewController*)vc {
    
    if (vc.presentedViewController) {
        
        // Return presented view controller
        return [self findBestViewController:vc.presentedViewController];
        
    } else if ([vc isKindOfClass:[UISplitViewController class]]) {
        
        // Return right hand side
        UISplitViewController* svc = (UISplitViewController*) vc;
        if (svc.viewControllers.count > 0)
            return [self findBestViewController:svc.viewControllers.lastObject];
        else
            return vc;
        
    } else if ([vc isKindOfClass:[UINavigationController class]]) {
        
        // Return top view
        UINavigationController* svc = (UINavigationController*) vc;
        if (svc.viewControllers.count > 0)
            return [self findBestViewController:svc.topViewController];
        else
            return vc;
        
    } else if ([vc isKindOfClass:[UITabBarController class]]) {
        
        // Return visible view
        UITabBarController* svc = (UITabBarController*) vc;
        if (svc.viewControllers.count > 0)
            return [self findBestViewController:svc.selectedViewController];
        else
            return vc;
        
    } else {
        
        // Unknown view controller type, return last child view controller
        return vc;
        
    }
    
}

-(UIViewController*) currentViewController {
    
    // Find best view controller
    UIViewController* viewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    return [self findBestViewController:viewController];
    
}

@end
