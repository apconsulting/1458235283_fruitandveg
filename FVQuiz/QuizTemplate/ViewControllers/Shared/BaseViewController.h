//
//  BaseViewController.h
//  QuizTemplate
//
//  Created by Uladzislau Yasnitski on 11/12/13.
//  Copyright (c) 2013 Uladzislau Yasnitski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMobileAds/GoogleMobileAds.h>
#import "UIFont+CustomFont.h"
#import "UIViewController+UpdateCoins.h"
#import "GetCoinsViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "AlertViewController.h"

@interface BaseViewController : UIViewController <GADBannerViewDelegate, GetCoinsViewDelegate, AlertViewControllerDelegate>

@property (nonatomic, strong) UIProgressView *coinsProgress;
@property (nonatomic, weak) IBOutlet UILabel *lbCoins;
@property (nonatomic, weak) IBOutlet UILabel *lbPoints;
@property (nonatomic, weak) IBOutlet UIButton *btnBack;
@property (nonatomic, weak) IBOutlet UIButton *btntopCoins;
@property (nonatomic, weak) IBOutlet UIImageView *imageTopView;
@property (nonatomic, weak) IBOutlet UIImageView *imageBackgroundFon;
@property (nonatomic, strong) GADBannerView *admobBannerView;

-(IBAction)didCoins;
-(void)showNoCoinsAlert;
-(void)removeBanner;
- (void) showInterstitial;
- (void)showAdWowFullSize;
- (void)showAdWowNotification;
- (IBAction)showVK:(id)sender;
- (void)showUnity;
- (void)showAdToApp;
- (void)showChartboost;
- (void)showPresentAlert;
- (void)showTapjoy;
- (void)showAdToAppVideo;

@end
