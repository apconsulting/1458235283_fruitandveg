//
//  GameModel.h
//  QuizTemplate
//
//  Created by Uladzislau Yasnitski on 13/11/13.
//  Copyright (c) 2013 Uladzislau Yasnitski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PlayingPerson.h"
#import "Person.h"
#import "QuestionNew.h"
#import "GameCenterManager.h"
#import "PlayingBot.h"
#import "CarQuiz-Prefix.pch"

@interface GameModel : NSObject <GameCenterManagerDelegate, PlayingBotDelegate>

@property (nonatomic, strong) NSArray *allQuestions;
@property (nonatomic, strong) PlayingPerson *playingPerson;
@property (nonatomic, strong) PlayingBot *playingBot;
@property (nonatomic) NSInteger openedCells;

@property (nonatomic, strong) NSString *idsString;
@property (nonatomic, assign) BOOL isStart;
@property (nonatomic, assign) BOOL IsServer;
@property (nonatomic, assign) NSInteger lossesCount;

@property (nonatomic, strong) NSMutableDictionary *myPlayingQuestions;
@property (nonatomic, strong) NSMutableDictionary *opponentPlayingQuestions;
@property (nonatomic, assign) NSInteger currentLevel;

+(GameModel*)sharedInstance;

-(void)authenticateLocalUser;
-(void)startWithQuestionIds:(NSString*)ids;
-(void)startWithOffline;
- (NSString*)leaderboardCategory;

-(BOOL)spendCoins:(NSInteger)coins;
- (void)earnCoins:(NSInteger)coins;

-(QuestionNew*)question;
-(NSInteger)questionIndex;
-(NSInteger)questionTime;

-(NSDictionary*)didForRightQuestionOffline;
-(void)didForHelpQuestionOffline;
- (void)finishOfflineWithSuccess:(BOOL)success;
-(NSDictionary*)didForRightQuestionOnlineWithTimerValue:(NSInteger)timerValue;
-(NSDictionary*)didForWrongQuestionOnlineWithTimerValue:(NSInteger)timerValue;
-(NSDictionary*)didForWrongQuestionOffline;

-(void)finishOnlineGame;
-(BOOL)isGameCenterUserAuthenticated;
-(void)startOnlineGameWithViewController:(UIViewController*)vc;
-(NSDictionary*)calculateResultWithBid:(NSInteger)bid;
-(void)startGameWithBot;
-(void)onlineGameStarted;

-(void)addPlayingQuestionsObject:(PlayingQuestion *)object;
-(void)addOpponentPlayingQuestionsObject:(PlayingQuestion *)object;
-(BOOL)isOpponentAlreadyPlayedQuestion:(NSInteger)qIndex;

-(NSInteger)numberOfRightPlayersAnswer;
-(NSInteger)numberOfRightBotAnswer;
-(BotResults)botShoudWin;
@end
