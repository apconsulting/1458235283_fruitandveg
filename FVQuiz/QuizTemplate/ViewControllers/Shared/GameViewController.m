//
//  GameViewController.m
//  QuizTemplate
//
//  Created by Uladzislau Yasnitski on 11/12/13.
//  Copyright (c) 2013 Uladzislau Yasnitski. All rights reserved.
//

#import "GameViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "CRFileUtils.h"
#import "SoundManager.h"

@interface GameViewController ()

@end

@implementation GameViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //    _gameModel = [[GameModel alloc] init];
    [SoundManager sharedManager].allowsBackgroundMusic = NO;
    [[SoundManager sharedManager] prepareToPlay];
    [self showQuestion];
    
    [self.btnQuestionIndex setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnQuestionIndex setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    
    self.btnQuestionIndex.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.btnQuestionIndex.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)playWrongSound
{
    [[SoundManager sharedManager] playSound:@"error.mp3" looping:NO];
}

-(void)playRightAnswerSound
{
    [[SoundManager sharedManager] playSound:@"rightAnswer.mp3" looping:NO];
}
- (IBAction)didQuestionIndex:(id)sender {
    
}

-(IBAction)didBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)showQuestion
{
}

-(void)questionDidAnswerQuestion:(QuestionNew *)question withAnswer:(QustionInfo *)answer{
    if ([answer.bIsRightAnswer boolValue])
    {
        
        [self didForRightAnswer];
    }
    else
    {
        
        [self didForWrongAnswer];
    }
}

-(void)questionDidHelp
{
    //tepmplate
}

-(BOOL)questionCanOpenCell
{
    //template
    return NO;
}


-(void)didForRightAnswer
{
}

-(void)didForWrongAnswer
{
}

-(void)alertDidDismissed:(AlertViewController *)alert
{
    if (alert.tag == 111)
    {
        // right answer
        [self updateCoinsForLabel:self.lbCoins withAnimation:YES];
        [self updateGameCenterPointsForLabel:self.lbPoints withAnimation:YES];
        
        double delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self showQuestion];
        });
        
        [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
        
    }
    else if (alert.tag == 222)
    {
        [self updateCoinsForLabel:self.lbCoins withAnimation:YES];
        
        [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
        double delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self showQuestion];
        });
        
    }
    else if (alert.tag == 666)
    {
        [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
        return;
    }
    if (alert.tag == 999) {
        [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
        [self.navigationController performSelector:@selector(popViewControllerAnimated:) withObject:@(YES) afterDelay:0.05];
    }
}

-(void)alertControllerDidClose:(AlertViewController *)alert
{
    if (alert.tag == 111)
    {
        // right answer
        [self updateCoinsForLabel:self.lbCoins withAnimation:YES];
        [self updateGameCenterPointsForLabel:self.lbPoints withAnimation:YES];
        [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
        
        double delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self showQuestion];
        });
        
    }
    else if (alert.tag == 222)
    {
        [self updateCoinsForLabel:self.lbCoins withAnimation:YES];
        [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
        double delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self showQuestion];
        });
        
    }
    else if (alert.tag == 666)
    {
        [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
        self.view.userInteractionEnabled = NO;
        
        double delayInSeconds = 1;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            self.view.userInteractionEnabled = YES;
            [self didCoins];
        });
        return;
    }
    if (alert.tag == 999) {
        [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
        [self.navigationController performSelector:@selector(popViewControllerAnimated:) withObject:@(YES) afterDelay:0.05];
    }
}

@end
