//
//  PresentCoinsManager.m
//  AudioQuiz
//
//  Created by Vladislav on 5/29/13.
//  Copyright (c) 2013 Vladislav. All rights reserved.
//

#import "PresentCoinsManager.h"
#import "AppSettings.h"
#import "NSDate+Additions.h"
#import "Localization.h"
#import "ConstantsAndMacros.h"

#import <UserNotifications/UserNotifications.h>

#define DAY_INTERVAL 60*60*24

@implementation PresentCoinsManager

+ (void)scheduleVK:(NSTimeInterval)timeInteval {
    if (![[AppSettings currentLanguage] isEqualToString:@"ru_RU"]){
        return;
    }
    
    NSDate *dateToFire = [[NSDate date] dateByAddingTimeInterval:timeInteval];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"10.0")) {
        
        UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
        //content.title = [NSString localizedUserNotificationStringForKey:@"Attention:" arguments:nil];
        content.body = [NSString localizedUserNotificationStringForKey:[[Localization instance] stringWithKey:@"txt_presentVK"]
                                                             arguments:nil];
        content.sound = [UNNotificationSound defaultSound];
        
        /// 4. update application icon badge number
        content.badge = @([UIApplication sharedApplication].applicationIconBadgeNumber + 1);
        
        //        UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger
        //                                                      triggerWithTimeInterval:timeInteval repeats:NO];
        //        UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:[[Localization instance] stringWithKey:@"txt_presentVKAction"]
        //                                                                              content:content trigger:trigger];
        //        /// 3. schedule localNotification
        //        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        //        [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
        //            if (!error) {
        //                NSLog(@"add NotificationRequest succeeded!");
        //            }else{
        //                NSLog(@"add NotificationRequest error! %@", error);
        //            }
        //        }];
        //
        //
        //        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        //        [calendar setTimeZone:[NSTimeZone localTimeZone]];
        //
        //        NSDateComponents *components = [calendar components:vkLocalPushInterval fromDate:dateToFire];
        //
        //        UNCalendarNotificationTrigger *triggerCalendar = [UNCalendarNotificationTrigger triggerWithDateMatchingComponents:components repeats:YES];
        
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitWeekday fromDate:dateToFire];
        UNCalendarNotificationTrigger *trigger = [UNCalendarNotificationTrigger triggerWithDateMatchingComponents:components repeats:YES];
        
        UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:[[Localization instance] stringWithKey:@"txt_presentVKAction"]
                                                                              content:content trigger:trigger];
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
            if (!error) {
                NSLog(@"add NotificationRequest succeeded!");
            }else{
                NSLog(@"add NotificationRequest error! %@", error);
            }
        }];
        
    }else{
        UILocalNotification *notification = [[UILocalNotification alloc] init];
        notification.fireDate = dateToFire;
        notification.timeZone = [NSTimeZone defaultTimeZone];
        notification.applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber + 1;
        notification.alertBody = [[Localization instance] stringWithKey:@"txt_presentVK"];
        notification.alertAction = [[Localization instance] stringWithKey:@"txt_presentVKAction"];
        notification.hasAction = YES;
        notification.repeatInterval = vkLocalPushInterval;
        
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    }
    
    
    
    [AppSettings setPresentVkDate:dateToFire];
}

+ (void)scheduleVideo:(NSTimeInterval)timeInteval {
    
    NSDate *dateToFire = [[NSDate date] dateByAddingTimeInterval:timeInteval];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"10.0")) {
        
        UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
        //content.title = [NSString localizedUserNotificationStringForKey:@"Attention:" arguments:nil];
        content.body = [NSString localizedUserNotificationStringForKey:[[Localization instance] stringWithKey:@"txt_presentRV"]
                                                             arguments:nil];
        content.sound = [UNNotificationSound defaultSound];
        
        /// 4. update application icon badge number
        content.badge = @([UIApplication sharedApplication].applicationIconBadgeNumber + 1);
        
        //        UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger
        //                                                      triggerWithTimeInterval:timeInteval repeats:NO];
        //        UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:[[Localization instance] stringWithKey:@"txt_presentRVAction"]
        //                                                                              content:content trigger:trigger];
        //        /// 3. schedule localNotification
        //        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        //        [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
        //            if (!error) {
        //                NSLog(@"add NotificationRequest succeeded!");
        //            }else{
        //                NSLog(@"add NotificationRequest error! %@", error);
        //            }
        //
        //        }];
        //
        //
        //        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        //        [calendar setTimeZone:[NSTimeZone localTimeZone]];
        //
        //        NSDateComponents *components = [calendar components:videoLocalPushInterval fromDate:dateToFire];
        
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitWeekday fromDate:dateToFire];
        UNCalendarNotificationTrigger *trigger = [UNCalendarNotificationTrigger triggerWithDateMatchingComponents:components repeats:YES];
        
        UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:[[Localization instance] stringWithKey:@"txt_presentRVAction"]
                                                                              content:content trigger:trigger];
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
            if (!error) {
                NSLog(@"add NotificationRequest succeeded!");
            }else{
                NSLog(@"add NotificationRequest error! %@", error);
            }
        }];
        
        
    }else{
        
        UILocalNotification *notification = [[UILocalNotification alloc] init];
        notification.fireDate = dateToFire;
        notification.repeatInterval = videoLocalPushInterval;
        notification.timeZone = [NSTimeZone defaultTimeZone];
        notification.applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber + 1;
        notification.alertBody = [[Localization instance] stringWithKey:@"txt_presentRV"];
        notification.alertAction = [[Localization instance] stringWithKey:@"txt_presentRVAction"];
        notification.hasAction = YES;
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
        
    }
    
    
    [AppSettings setPresentVideoDate:dateToFire];
}

+ (void)setupCoinsPresent:(NSTimeInterval)timeInteval {
    [AppSettings setPresentDate:[NSDate ServerDate]];
    
    
    NSDate *dateToFire = [[NSDate date] dateByAddingTimeInterval:timeInteval];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"10.0")) {
        
        UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
        //content.title = [NSString localizedUserNotificationStringForKey:@"Attention:" arguments:nil];
        content.body = [NSString localizedUserNotificationStringForKey:[[Localization instance] stringWithKey:@"txt_present"]
                                                             arguments:nil];
        
        content.sound = [UNNotificationSound defaultSound];
        
        /// 4. update application icon badge number
        content.badge = @([UIApplication sharedApplication].applicationIconBadgeNumber + 1);
        
        //        UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger
        //                                                      triggerWithTimeInterval:timeInteval repeats:NO];
        //        UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"View"
        //                                                                              content:content trigger:trigger];
        //        /// 3. schedule localNotification
        //        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        //        [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
        //            if (!error) {
        //                NSLog(@"add NotificationRequest succeeded!");
        //            }else{
        //                NSLog(@"add NotificationRequest error! %@", error);
        //            }
        //
        //        }];
        
        //        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        //        [calendar setTimeZone:[NSTimeZone localTimeZone]];
        //
        //        NSDateComponents *components = [calendar components:NSCalendarUnitMinute/*coinsLocalPushInterval*/ fromDate:[NSDate date]];
        //
        //        UNCalendarNotificationTrigger *triggerCalendar = [UNCalendarNotificationTrigger triggerWithDateMatchingComponents:components repeats:YES];
        
        //        UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:timeInteval repeats:YES];
        
        //coinsLocalPushInterval
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitHour | NSCalendarUnitMinute fromDate:dateToFire];
//        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitSecond fromDate:dateToFire];
        //        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay fromDate:dateToFire];
        UNCalendarNotificationTrigger *trigger = [UNCalendarNotificationTrigger triggerWithDateMatchingComponents:components repeats:YES];
        
        UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"View"
                                                                              content:content trigger:trigger];
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
            if (!error) {
                NSLog(@"setupCoinsPresent add NotificationRequest succeeded!");
            }else{
                NSLog(@"setupCoinsPresent add NotificationRequest error! %@", error);
            }
        }];
        
    }else{
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        
        if (!localNotification)
            return;
        
        [localNotification setFireDate:dateToFire];
        [localNotification setTimeZone:[NSTimeZone defaultTimeZone]];
        
        [localNotification setAlertBody:[[Localization instance] stringWithKey:@"txt_present"]];
        [localNotification setAlertAction:@"View"];
        [localNotification setHasAction:YES];
        localNotification.repeatInterval = coinsLocalPushInterval;
        localNotification.applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber + 1;
        [localNotification setSoundName:nil];
        
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
}

+ (BOOL)appLaunch {
    BOOL willPushLocalNotification = YES;
    NSDate *date = [AppSettings presentDate];
    if (date) {
        NSInteger days = [[NSDate date] daysAfterDate:date];
        if (days) {
            
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"10.0")) {
                [[UNUserNotificationCenter currentNotificationCenter] removeAllPendingNotificationRequests];
            }else{
                NSArray *arrayOfLocalNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications] ;
                for (UILocalNotification *localNotification in arrayOfLocalNotifications) {
                    if ([localNotification.alertAction isEqualToString:@"View"]) {
                        [[UIApplication sharedApplication] cancelLocalNotification:localNotification] ;
                    }
                }
            }
            [self setupCoinsPresent:3];
        } else {
            willPushLocalNotification = NO;
        }
        
        [AppSettings setPresentDate:[NSDate date]];
        
        return willPushLocalNotification;
    }
    
    [self setupCoinsPresent:3];
    [self scheduleVideo:videoLocalPushOffset];
    [self scheduleVK:vkLocalPushOffset];
    
    return willPushLocalNotification;
}

+ (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime {
    
    NSDate *startdate = fromDateTime;
    NSDate *toDate = toDateTime;
    
    int i = [startdate timeIntervalSince1970];
    int j = [toDate timeIntervalSince1970];
    
    double X = j-i;
    int days=(int)(((double)X/(3600.0*24.00))); // two days between
    return days;
    
}


@end
