//
//  PersonInitializer.h
//  AudioQuiz
//
//  Created by Vladislav on 4/8/13.
//  Copyright (c) 2013 Vladislav. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PersonInitializer : NSObject
+(BOOL)initializePersonWithEarnedCoins:(NSInteger)coins boungtCoins:(NSInteger)bought questionIndex1:(NSInteger)qIndex1
                        questionIndex2:(NSInteger)qIndex2 questionIndex3:(NSInteger)qIndex3;

@end
