//
//  Question.m
//  QuizTemplate
//
//  Created by Uladzislau Yasnitski on 11/12/13.
//  Copyright (c) 2013 Uladzislau Yasnitski. All rights reserved.
//

#import "QuestionNew.h"
#import "QustionInfo.h"
#import "OpenedCell.h"
#import "NSMutableArray+Shuffling.h"
#import "CRFileUtils.h"

@implementation QuestionNew

@dynamic questionId;
@dynamic questionAnswers;
@dynamic openedCells;
@dynamic questionRemoveAllCells;
@dynamic questionTour;
@dynamic questionImageId;
@dynamic questionText;
@dynamic answer;
@dynamic level;

-(NSString*)questionImagePath
{
    return [CRFileUtils resourcePath:[NSString stringWithFormat:@"/Content/%@-%@.jpg",self.questionTour,self.questionImageId]];
    
    //    NSLog(@"Image Path - %@",[CRFileUtils resourcePath:[NSString stringWithFormat:@"/Content/%@.jpg",self.questionId]]);
    //    return [CRFileUtils resourcePath:[NSString stringWithFormat:@"/Content/%@.jpg",self.questionId]];
}
+(NSArray*)questionsByIndexes:(NSString *)ids level:(NSInteger)level
{
    
    //    NSLog(@"Qestions ids %@",ids);
    //    ids = @"251:251:251:251:251";
    NSArray *idsArray = [ids componentsSeparatedByString:@":"];
    
    NSMutableArray *result = [NSMutableArray array];
    
    if ([idsArray count] == 5)
    {
        for (NSString *ID in idsArray)
        {
            [result addObject:[self questionById:[NSNumber numberWithInt:[ID intValue]] level:level]];
        }
    }
    else
    {
        for (NSInteger i = [result count]; i < 5; i++)
        {
            [result addObject:[self questionById:[NSNumber numberWithInt:(arc4random() % [[QuestionNew allQuestions:level] count])] level:level]];
        }
    }
    return result;
    
}

+(QuestionNew*)questionById:(NSNumber*)index  level:(NSInteger)level
{
    NSFetchRequest* request = [DELEGATE.managedObjectModel fetchRequestFromTemplateWithName:@"question_by_index" substitutionVariables:@{@"questionId":index, @"level" : @(level)}];
    
    NSError* error = nil;
    if ([[DELEGATE.managedObjectContext executeFetchRequest:request error:&error] lastObject])
        return [[DELEGATE.managedObjectContext executeFetchRequest:request error:&error] lastObject];
    else
        return [self questionById:[NSNumber numberWithInt:(arc4random() % [[QuestionNew allQuestions:level] count])] level:level];
}



+(NSArray*)allQuestions:(NSInteger)level
{
    NSFetchRequest* request = [DELEGATE.managedObjectModel fetchRequestFromTemplateWithName:@"question_sorted_by_id" substitutionVariables:@{@"level" : @(level)}];
    
    [request setSortDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"questionId" ascending:YES]]];
    NSError* error = nil;
    return [DELEGATE.managedObjectContext executeFetchRequest:request error:&error];
}

-(NSArray*)allQuestionInfos
{
    NSMutableArray *array = [[self.questionAnswers allObjects] mutableCopy];
    [array shuffle];
    
    return array;
}

- (NSDictionary*)wkDictionary {
    //    NSData *data = [NSData dataWithContentsOfFile:self.questionImagePath];
    NSMutableDictionary *serialized = [@{@"questionId":self.questionId, @"questionText" : self.questionText} mutableCopy];
    NSMutableArray *info = [NSMutableArray array];
    [self.questionAnswers enumerateObjectsUsingBlock:^(id obj, BOOL *stop) {
        [info addObject:[obj wkDictionary]];
    }];
    [serialized setValue:info forKey:@"answers"];
    return serialized;
}
@end
