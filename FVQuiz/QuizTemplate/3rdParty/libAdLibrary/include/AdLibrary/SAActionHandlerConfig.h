#import <Foundation/Foundation.h>
#import "SAActionHandler.h"


@interface SAActionHandlerConfig : NSObject<SAActionHandler>

- (NSDictionary *)JSONFromString:(NSString *)string;

@end