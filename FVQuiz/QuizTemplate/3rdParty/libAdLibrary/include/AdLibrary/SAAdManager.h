
#import <Foundation/Foundation.h>

typedef enum {
    SAAdBannerTypeFullscreen
} SAAdBannerType;


@interface SAAdManager : NSObject

+ (void)showAdBannerForAppID:(NSString *)appID type:(SAAdBannerType)type;
+ (NSString *)libraryVersion;

@end
