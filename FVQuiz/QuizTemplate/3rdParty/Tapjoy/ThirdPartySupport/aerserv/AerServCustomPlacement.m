// Copyright (C) 2015 by Tapjoy Inc.
//
// This file is part of the Tapjoy SDK.
//
// By using the Tapjoy SDK in your software, you agree to the terms of the Tapjoy SDK License Agreement.
//
// The Tapjoy SDK is bound by the Tapjoy SDK License Agreement and can be found here: https://www.tapjoy.com/sdk/license


#import "AerServCustomPlacement.h"
#import <AerServSDK/AerServSDK.h>

@interface AerServCustomPlacement () <ASInterstitialViewControllerDelegate>

@property (nonatomic, strong) ASInterstitialViewController *adController;

@end



@implementation AerServCustomPlacement

- (void)requestContentWithCustomPlacementParams:(NSDictionary *)params
{
    NSString *aerServPlacementID = [NSString stringWithFormat:@"%@", [params objectForKey:@"placement_id"]];
    self.adController = [[ASInterstitialViewController alloc] viewControllerForPlacementID:aerServPlacementID withDelegate:self];
    
    self.adController.isPreload = YES;
    [self.adController loadAd];
}

- (void)showContentWithViewController:(UIViewController*)viewController
{
    [self.adController showFromViewController:viewController];
}

- (void)dealloc
{
    self.adController = nil;
}


#pragma mark - ASInterstitialViewControllerDelegate

- (void)interstitialViewControllerDidPreloadAd:(ASInterstitialViewController *)viewController {
    NSLog(@"AerServ interstitial loaded");
    [self.delegate customPlacement:self didLoadAd:nil];
}

- (void)interstitialViewControllerAdLoadedSuccessfully:(ASInterstitialViewController *)viewController {
    NSLog(@"AerServ interstitial loaded");
    [self.delegate customPlacement:self didLoadAd:nil];
}

- (void)interstitialViewControllerAdFailedToLoad:(ASInterstitialViewController*)viewController withError:(NSError*)error {
    NSLog(@"AerServ interstitial failed to load with error:%@", error);
    [self.delegate customPlacement:self didFailWithError:error];
}

-(void)interstitialViewControllerDidAppear:(ASInterstitialViewController *)viewController{
    NSLog(@"AerServ interstitial shown");
    [self.delegate customPlacementContentDidAppear:self];
}

- (void)interstitialViewControllerAdWasTouched:(ASInterstitialViewController *)viewController {
}

- (void)interstitialViewControllerDidDisappear:(ASInterstitialViewController *)viewController {
    self.adController = nil;
    NSLog(@"AerServ interstitial dismissed");
    [self.delegate customPlacementContentDidDisappear:self];
}

- (void)interstitialViewControllerDidVirtualCurrencyReward:(ASInterstitialViewController *)viewController vcData:(NSDictionary *)vcData
{
    NSLog(@"AerServ interstitial did reward");
    [self.delegate customPlacement:self shouldReward:[vcData objectForKey:@"name"] amount:[[vcData objectForKey:@"rewardAmount"] intValue]];
}

@end
