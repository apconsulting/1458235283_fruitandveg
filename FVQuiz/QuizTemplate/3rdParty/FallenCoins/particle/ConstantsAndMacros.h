//
// Particlesconstants.h
//  Particles
//
//  Created by Jeff LaMarche on 12/30/08.
//  Copyright Jeff LaMarche Consulting 2008. All rights reserved.
//

// How many times a second to refresh the screen
#if TARGET_OS_IPHONE
#define kRenderingFrequency 15.0
#elif TARGET_IPHONE_SIMULATOR
#define kRenderingFrequency 30.0
#endif
// For setting up perspective, define near, far, and angle of view
#define kZNear			0.01
#define kZFar			1000.0
#define kFieldOfView	45.0

#define colorBackgroundSubsribeForIpad [UIColor colorWithWhite:0.0 alpha:0.5]
#define urlENG @"http://iosgames.ru/fruit-and-veg/"
#define urlRUS @"http://iosgames.ru/fruit-and-veg/"

#define urlTW @"http://twitter.com/game4ios"
#define urlFB @"https://www.facebook.com/game4ios"
#define urlVK @"https://vk.com/iosgamesru"

#define VkAppID @"6226964"
#define VkAppKey @"AjERMkJpcUQe4PNP7r1n"

#define urlOtherQu @"http://iosgames.ru/games.html"
#define urlOtherQuEN @"https://itunes.apple.com/us/artist/iplanetsoft/id483944805"

#define TapJoyPlace @"Pause"
#define TapJoyPlace1 @"Offline"
#define TapJoyPlace2 @"Pause"

// Macros
#define DEGREES_TO_RADIANS(__ANGLE__) ((__ANGLE__) / 180.0 * M_PI)
#define IOS_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]

static BOOL showAdToAppVideoOnEven = NO;
static BOOL showAdToAppOnEven = NO;

static NSInteger chartboostReward = 1;
static NSInteger unityReward = 50;

static NSInteger vkAuthErnedCoins = 0;
static NSInteger vkShareErnedCoins = 30;
static NSInteger vkSibscribeErnedCoins = 20;
static NSInteger vkInviteFriendErnedCoins = 40;

static NSInteger fbShareErnedCoins = 30;
static NSInteger twShareErnedCoins = 30;

static NSString *const urlAppForVK = @"https://itunes.apple.com/ru/app/apple-store/id804942069?pt=879222&ct=vkf";//@"https://itunes.apple.com/us/app/car-quiz!/id804942069?mt=8";
static NSString *const shareLinkTextToVK = @"Ссылка в App Store:";
static NSString *const shareTextToVK = @"Я играю в Угадай авто! Отличная викторина! Присоединяйтесь!";

static NSCalendarUnit const coinsLocalPushInterval = NSCalendarUnitDay;//раз в день
static NSCalendarUnit const vkLocalPushInterval = NSCalendarUnitWeekOfMonth;//раз в неделю
static NSCalendarUnit const videoLocalPushInterval = NSCalendarUnitWeekOfMonth;//раз в неделю

static NSTimeInterval const vkLocalPushOffset = 424800; //118 часов
static NSTimeInterval const videoLocalPushOffset = 165600; //46 часов

//static NSTimeInterval const vkLocalPushOffset = 3600 * 24 * 5 + 120; //пять дней + 2 часа
//static NSTimeInterval const videoLocalPushOffset = 3600 * 24 * 3 + 120; //три дня + 2 часа
