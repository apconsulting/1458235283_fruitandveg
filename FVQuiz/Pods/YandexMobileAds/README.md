# Yandex Advertising Network Mobile
This package contains Yandex Advertising Network Mobile SDK and source code of samples of SDK usage.

## [**Download**](https://storage.mds.yandex.net/get-ads-mobile-sdk/212922/YandexMobileAds-2.11.0-ios-ea6f0b6f-f4b3-415c-bf58-22806e8c94b4.zip) latest SDK archive

## Documentation
Documentation could be found at the [official website][DOCUMENTATION]

## License
EULA is available at [EULA website][LICENSE] 

## Quick start
1. Install [CocoaPods] to manage project dependencies, if you haven't done it yet.

2. Go to one of example projects:
#### Objective C
  * /Examples/ObjectiveC/BannerExample
  * /Examples/ObjectiveC/GDPRExample
  * /Examples/ObjectiveC/InterstitialExample
  * /Examples/ObjectiveC/NativeExample
  * /Examples/ObjectiveC/NativeTemplatesExample
  * /Examples/ObjectiveC/NativeTemplatesTableViewExample
  * /Examples/ObjectiveC/RewardedExample
  * /Examples/ObjectiveC/VideoExample
  * /Examples/ObjectiveC/AdMobBannerAdapterExample
  * /Examples/ObjectiveC/AdMobRewardedAdapterExample
  * /Examples/ObjectiveC/MoPubBannerAdapterExample
  * /Examples/ObjectiveC/MoPubRewardedAdapterExample
  * /Examples/ObjectiveC/AdFoxNativeExample
  * /Examples/ObjectiveC/AdFoxMediationBannerExample
  * /Examples/ObjectiveC/AdFoxMediationInterstitialExample
  * /Examples/ObjectiveC/AdFoxMediationNativeExample
  * /Examples/ObjectiveC/AdFoxMediationRewardedExample
  
#### Swift
  * /Examples/Swift/BannerExample
  * /Examples/Swift/InterstitialExample
  * /Examples/Swift/NativeTemplatesExample
  * /Examples/Swift/RewardedExample
  * /Examples/Swift/AdFoxMediationRewardedExample

3. Install dependencies:
```pod install```

4. Open project workspace.

5. Build and run.

[DOCUMENTATION]: https://tech.yandex.ru/mobile-ads/
[LICENSE]: https://yandex.com/legal/mobileads_sdk_agreement/
[CocoaPods]: http://cocoapods.org/
